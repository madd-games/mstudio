#! /usr/bin/env python3
"""
	Madd Studio

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

# First check if we are in "wrapper mode"
from subprocess import Popen
import sys, os
if len(sys.argv) > 2:
	if sys.argv[1] == "--wrap":
		cmdstr = " ".join(sys.argv[2:])
		print("\u001b[1;34m%s\u001b[0m" % cmdstr)
		try:
			p = Popen(sys.argv[2:])
			status = p.wait()
			
			if status == 0:
				print("\u001b[1;32mCommand completed successfully\u001b[0m")
			else:
				print("\u001b[1;31mCommand failed: exit code %d\u001b[0m" % status)
		except Exception as e:
			print("\u001b[1;31mException thrown: %s\u0001b[0m" % str(e))
		input("Press ENTER to exit:")
		exit()

# This must be imported first because of reasons
from mstudio.UserConfig import *

# Now everything else
from mstudio.MainFrame import MainFrame
from mstudio.EditorFile import EditorFile
from mstudio.Plugin import *
import wx

# Load plugins
InitPlugins()

# Start everything
main = MainFrame()
app.MainLoop()
