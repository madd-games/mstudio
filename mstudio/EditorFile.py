"""
	Madd Studio

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

from mstudio.EditorMode import EditorMode
from mstudio.EditorModeList import editorModeList
from mstudio.IconList import fileIcons
from mstudio.UserConfig import *
import sys, os
import wx

GLOBAL_FILE_LIST = {}

"""Represents a file open in the editor.

"""
class EditorFile:
	def __init__(self, nav, project, item, filename):
		self.filename = filename
		GLOBAL_FILE_LIST[filename] = self
		self.loaded = False
		self.content = None
		self.dirty = False
		self.mode = EditorMode()
		self.nav = nav
		self.project = project
		self.item = item
		self.lastCursorPos = 0
		self.deleted = False
		self.mtime = os.path.getmtime(filename)
		
		iconName = "Unknown"
		for suffix, defaultIcon in fileIcons.items():
			if filename.endswith(suffix):
				iconName = defaultIcon
		
		self.defaultIconName = iconName
		nav.SetItemImage(item, nav.icons[iconName], wx.TreeItemIcon_Normal)
	
	def setMode(self, mode):
		self.mode = mode
		iconName = mode.getIconName(self.filename, self.content)
		if iconName == "Unknown":
			iconName = self.defaultIconName
		self.nav.SetItemImage(self.item, self.nav.icons[iconName], wx.TreeItemIcon_Normal)
		
	def getMode(self):
		return self.mode

	def read(self):
		if not self.loaded:
			try:
				f = open(self.filename, "r")
				self.content = f.read()
				f.close()
				self.loaded = True
				self.mtime = os.path.getmtime(self.filename)
				
				try:
					self.setMode(editorModeList[userConfig["CodeEditor.FileModes"][self.filename]])
				except KeyError:
					for mode in editorModeList.values():
						if mode.isForFile(self.filename, self.content):
							self.setMode(mode)
							break
			except Exception as e:
				wx.MessageBox("Failed to load the file: " + str(e), "Madd Studio", wx.ICON_ERROR)
				return None
		
		return self.content
	
	def update(self, content):
		self.read()
		
		if content != self.content:
			self.loaded = True
			self.content = content
			self.dirty = True
			self.nav.SetItemBold(self.item, True)
			self.nav.SetItemText(self.item, self.nav.GetItemText(self.item))

	def save(self):
		if self.deleted:
			return True

		if self.dirty:
			try:
				f = open(self.filename, "w")
				f.write(self.content)
				f.close()
				self.dirty = False
				self.nav.SetItemBold(self.item, False)
				self.nav.SetItemText(self.item, self.nav.GetItemText(self.item))
				return True
			except:
				wx.MessageBox("Failed to save the file.", "Madd Studio", wx.ICON_ERROR)
				return False
		return True
	
	def delete(self):
		if wx.MessageBox("Are you sure you want to PERMANENTLY DELETE %s?" % self.filename, "Madd Studio", wx.ICON_WARNING | wx.YES_NO) == wx.YES:
			os.remove(self.filename)
	
	"""Called at regular intervals to check for filesystem changes."""
	def onTick(self):
		if self.loaded:
			upToDate = True
			try:
				currentMTime = os.path.getmtime(self.filename)
				if currentMTime > self.mtime:
					upToDate = False
			except:
				upToDate = False
			
			if not upToDate:
				self.loaded = False
