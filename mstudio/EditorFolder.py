"""
	Madd Studio

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

from mstudio.EditorFile import EditorFile
import sys, os
import wx

"""Represents a directory (including the root of a project) in the navigator.

"""
class EditorFolder:
	def __init__(self, nav, project, item, dirname):
		self.project = project
		self.item = item
		self.dirname = dirname
		self.nav = nav
		self.mtime = os.path.getmtime(dirname)
		self.folders = []
		self.files = []
		
		nav.SetItemImage(item, nav.icons["Folder"], wx.TreeItemIcon_Normal)
		
		try:
			files = os.listdir(dirname)
			files.sort()
			for name in files:
				if self.isHiddenName(name):
					continue
				fullpath = os.path.join(dirname, name)
				if os.path.isdir(fullpath):
					subitem = nav.AppendItem(item, name)
					folder = EditorFolder(nav, project, subitem, fullpath)
					self.folders.append(folder)
					nav.SetItemData(subitem, folder)
				else:
					subitem = nav.AppendItem(item, name)
					efile = EditorFile(nav, project, subitem, fullpath)
					self.files.append(efile)
					nav.SetItemData(subitem, efile)

		except Exception as e:
			print("ERROR: {0}".format(e))
			wx.MessageBox("Failed to scan directory %s" % dirname, "Madd Studio", wx.ICON_ERROR)
	
	def isHiddenName(self, name):
		return name == "." or name == ".." or name == ".git"
		
	def saveAll(self):
		for folder in self.folders:
			if not folder.saveAll():
				return False
		
		for efile in self.files:
			if not efile.save():
				return False
		
		return True

	def hasDirty(self):
		for folder in self.folders:
			if folder.hasDirty():
				return True
		
		for efile in self.files:
			if efile.dirty and not efile.deleted:
				return True
		
		return False

	def newFolder(self):
		dlg = wx.TextEntryDialog(None, "Please enter the name of a folder to create under %s" % self.dirname, "Madd Studio")
		if dlg.ShowModal() == wx.ID_CANCEL:
			return
		
		name = dlg.GetValue()
		if "\\" in name or "/" in name:
			wx.MessageBox("Slashes are not allowed in a folder name.", "Madd Studio", wx.ICON_ERROR)
			return
		
		fullpath = os.path.join(self.dirname, name)
		try:
			os.mkdir(fullpath)
		except:
			wx.MessageBox("Failed to create folder %s" % fullpath, "Madd Studio", wx.ICON_ERROR)
			return
		
		subitem = self.nav.AppendItem(self.item, name)
		folder = EditorFolder(self.nav, self.project, subitem, fullpath)
		self.folders.append(folder)
		self.nav.SetItemData(subitem, wx.TreeItemData(folder))

	def newFile(self):
		dlg = wx.TextEntryDialog(None, "Please enter the name of a file to create under %s" % self.dirname, "Madd Studio")
		if dlg.ShowModal() == wx.ID_CANCEL:
			return
		
		name = dlg.GetValue()
		if "\\" in name or "/" in name:
			wx.MessageBox("Slashes are not allowed in a file name.", "Madd Studio", wx.ICON_ERROR)
			return
		
		fullpath = os.path.join(self.dirname, name)
		if os.path.exists(fullpath):
			wx.MessageBox("File %s already exists" % fullpath, "Madd Studio", wx.ICON_ERROR)
			return
		
		try:
			open(fullpath, "w").close()
		except:
			wx.MessageBox("Failed to create file %s" % fullpath, "Madd Studio", wx.ICON_ERROR)
			return

		subitem = self.nav.AppendItem(self.item, name)
		efile = EditorFile(self.nav, self.project, subitem, fullpath)
		self.files.append(efile)
		self.nav.SetItemData(subitem, efile)

	"""Called at regular intervals, to check for filesystem changes."""
	def onTick(self):
		if not self.nav.IsExpanded(self.item):
			# no need to check anything if not expanded
			return

		entList = []
		
		newFolderList = []
		for folder in self.folders:
			if os.path.exists(folder.dirname) and os.path.isdir(folder.dirname):
				newFolderList.append(folder)
				folder.onTick()
				entList.append(os.path.basename(folder.dirname))
			else:
				self.nav.Delete(folder.item)
		self.folders = newFolderList
		
		newFileList = []
		for efile in self.files:
			if os.path.exists(efile.filename) and os.path.isfile(efile.filename):
				newFileList.append(efile)
				efile.onTick()
				entList.append(os.path.basename(efile.filename))
			else:
				self.nav.Delete(efile.item)
		self.files = newFileList
		
		try:
			files = os.listdir(self.dirname)
			files.sort()
			for name in files:
				if self.isHiddenName(name):
					continue
				if name not in entList:
					fullpath = os.path.join(self.dirname, name)
					if os.path.isdir(fullpath):
						subitem = self.nav.AppendItem(self.item, name)
						folder = EditorFolder(self.nav, self.project, subitem, fullpath)
						self.folders.append(folder)
						self.nav.SetItemData(subitem, folder)
					else:
						subitem = self.nav.AppendItem(self.item, name)
						efile = EditorFile(self.nav, self.project, subitem, fullpath)
						self.files.append(efile)
						self.nav.SetItemData(subitem, efile)
		except Exception as e:
			print("ERR: %s" % str(e))
