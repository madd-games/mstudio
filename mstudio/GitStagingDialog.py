"""
	Madd Studio

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

import wx

"""Git staging dialog."""
class GitStagingDialog(wx.Dialog):
	def __init__(self, mainFrame):
		wx.Dialog.__init__(self, mainFrame)
		self.SetTitle("Stage & commit...")
		self.mainFrame = mainFrame
		
		masterSizer = wx.BoxSizer(wx.VERTICAL)
		masterSizer.SetMinSize(wx.Size(600, 300))

		stagingAreaSizer = wx.BoxSizer(wx.HORIZONTAL)
		masterSizer.Add(stagingAreaSizer, 2, wx.ALL | wx.EXPAND, 10)
		
		self.lstFiles = wx.CheckListBox(self)
		stagingAreaSizer.Add(self.lstFiles, 1, wx.RIGHT | wx.EXPAND, 10)
		
		self.txtDiff = wx.TextCtrl(self, style=wx.TE_MULTILINE|wx.TE_READONLY)
		stagingAreaSizer.Add(self.txtDiff, 1, wx.EXPAND, 10)

		commitAreaSizer = wx.BoxSizer(wx.HORIZONTAL)
		masterSizer.Add(commitAreaSizer, 0, (wx.ALL ^ wx.TOP) | wx.EXPAND, 10)
		
		self.txtShortMsg = wx.TextCtrl(self)
		commitAreaSizer.Add(self.txtShortMsg, 1, wx.RIGHT, 10)
		self.txtShortMsg.SetHint("Short commit message...")
		
		self.ID_COMMIT = self.NewControlId()
		self.btnCommit = wx.Button(self, self.ID_COMMIT, label="Commit")
		commitAreaSizer.Add(self.btnCommit, 0, 0, 0)
		self.btnCommit.Disable()
		
		self.txtLongMsg = wx.TextCtrl(self, style=wx.TE_MULTILINE)
		masterSizer.Add(self.txtLongMsg, 1, (wx.ALL ^ wx.TOP) | wx.EXPAND, 10)
		
		btnSizer = self.CreateSeparatedButtonSizer(wx.CLOSE)
		masterSizer.Add(btnSizer, 0, (wx.ALL ^ wx.TOP) | wx.EXPAND, 10)
		
		masterSizer.SetSizeHints(self)
		self.SetSizer(masterSizer)
		
		self.reloadGitInfo()
		
		self.Bind(wx.EVT_LISTBOX, self.onSelect)
		self.Bind(wx.EVT_CHECKLISTBOX, self.onCheck)
		self.Bind(wx.EVT_BUTTON, self.onCommit, id = self.ID_COMMIT)
		self.Bind(wx.EVT_TEXT, self.onTextChanged)
	
	def reloadGitInfo(self):
		try:
			statusString = self.mainFrame.git("status", "-s", "-uall")
		except Exception as e:
			wx.MessageBox(str(e), "Madd Studio", wx.ICON_ERROR)
			self.EndModal(0)
			return
		
		allFiles = []
		stagedFiles = []
		
		for line in statusString.splitlines():
			if len(line) > 3:
				status = line[:2]
				filename = line[3:]
				allFiles.append(filename)
				
				if status[1] == " ":
					stagedFiles.append(filename)
		
		self.lstFiles.Clear()
		self.lstFiles.Append(allFiles)
		self.lstFiles.SetCheckedStrings(stagedFiles)
	
	def getDiffText(self, filename):
		try:
			return self.mainFrame.git("diff", "HEAD", "--", filename)
		except Exception as e:
			return "ERROR: Cannot get diff:\n" + str(e)
		
	def onSelect(self, e):
		i = self.lstFiles.GetSelection()
		if i != -1:
			name = self.lstFiles.GetString(i)
			self.txtDiff.SetValue(self.getDiffText(name))
	
	def assertGit(self, *args):
		try:
			self.mainFrame.git(*args)
		except Exception as e:
			wx.MessageBox(str(e), "Madd Studio", wx.ICON_ERROR)
			self.EndModal(0)
			
	def onCheck(self, e):
		i = e.GetInt()
		name = self.lstFiles.GetString(i)
		checked = self.lstFiles.IsChecked(i)
		
		if checked:
			# was unchecked, now checked; so git add
			self.assertGit("add", name)
		else:
			# was checked, now unchecked; so uncheck with reset
			self.assertGit("reset", "--", name)
	
	def onTextChanged(self, e):
		if self.txtShortMsg.GetValue() != "":
			self.btnCommit.Enable()
		else:
			self.btnCommit.Disable()
		
	def onCommit(self, e):
		args = ["-m", self.txtShortMsg.GetValue()]
		
		longDesc = self.txtLongMsg.GetValue()
		if longDesc != "":
			for line in longDesc.splitlines():
				args.extend(("-m", line))
		
		self.assertGit("commit", *args)
		self.txtShortMsg.SetValue("")
		self.txtLongMsg.SetValue("")
		self.reloadGitInfo()
		self.btnCommit.Disable()