"""
	Madd Studio

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

from mstudio.Tokenizer import *

"""Represents a token in semtnatic highlighting."""
class SemanticToken:
	def __init__(self, startPos, length, attrName):
		self.startPos = startPos
		self.length = length
		self.attrName = attrName

"""Represents an editor mode.

There is basically one mode for every programming language. Children of this class define how to
color the syntax, set up suggestions, etc.
"""
class EditorMode:
	def __init__(self):
		self.stateTable = [[]]

	"""Called whenever a file is first laoded and its mode must be determined.
	
	Return True if the file name or contents match this mode, otherwise False.
	"""
	def isForFile(self, filename, content):
		return False
	
	"""Called whenever code was updated and needs to be colorized.
	
	'toker' is the tokenizer referring to the code, and is in whatever state is applicable (it
	might not be asking to colorize from the very beginning!). 'editor' is the editor itself, and
	can be used to obtain attribute values.
	"""
	def colorize(self, toker):
		while not toker.end():
			table = self.stateTable[toker.state]
			bestTransition = None
			
			for pattern, attr, newState in table:
				l = toker.hasMatch(pattern)
				if l != 0:
					if (bestTransition is None) or (bestTransition[1] < l):
						bestTransition = (attr, l, newState)
			
			if bestTransition is None:
				toker.skip(1)
			else:
				attr, l, newState = bestTransition
				if attr is None:
					toker.skip(l)
				else:
					toker.emit(l, attr, newState)
	
	"""Called whenever ENTER is pressed. Helps to handle auto-indentation etc.
	
	This method is given the text before and after the caret, and must return the tuple:
	(before, after). "before" is a string which is inserted at the caret, and then the caret
	is moved to be at the end of "before"; then "after" is inserted, but without moving the
	caret forward.
	"""
	def getContextualNewline(self, before, after):
		return ("", "")
	
	"""Called whenever text is changed. Helps to handle auto-closure of paranthesis etc.
	
	This method is given the text before and after the caret AFTER whatever text has been inserted.
	Then, this method returns (beforeText, afterText), and beforeText is inserted at the caret position,
	followed by afterText, and the caret is placed between the two.
	"""
	def getContextualAutocomplete(self, before, after):
		if before.endswith("\n"):
			return self.getContextualNewline(before[:-1], after)
		else:
			return ("", "")
	
	"""Return the icon name to use for the speicfied file.
	
	'filename' is the name of the file (full path), and 'content' is the file content."""
	def getIconName(self, filename, content):
		return "Unknown"
	
	"""Execute the semantic highlighter.
	
	'buildDir' is the path to the build directory.
	'fileBaseName' is the base name (final component) of the path to the file.
	'content' is the content of the file to highlight.
	'callback' is a function which must be called with an array of SemanticToken objects,
		once highlighting is complete. This function may be called from another thread."""
	def beginSemanticHighlight(self, buildDir, fileBaseName, content, callback):
		pass