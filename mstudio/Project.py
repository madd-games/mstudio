"""
	Madd Studio

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

from mstudio.EditorFolder import EditorFolder
import sys, os
import wx

"""Represents a project.

"""
class Project:
	def __init__(self, nav, name, info):
		self.nav = nav
		self.name = name
		self.info = info
		item = nav.AppendItem(nav.root, name)
		try:
			self.folder = EditorFolder(nav, self, item, info["ProjectDir"])
		except Exception as e:
			nav.Delete(item)
			raise e
		nav.SetItemData(item, self.folder)
		nav.SetItemImage(item, nav.icons["Project"], wx.TreeItemIcon_Normal)
		
		self.rootItem = item

	"""Return true if this project contains dirty files (unsaved changes)."""
	def hasDirty(self):
		return self.folder.hasDirty()

	def saveAll(self):
		return self.folder.saveAll()
	
	"""Called at regular intervals, to check for filesystem changes."""
	def onTick(self):
		self.folder.onTick()

	"""Get the root directory of this project."""
	def getRootDir(self):
		return self.folder.dirname
