"""
	Madd Studio

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

import wx

"""Git merge dialog."""
class GitMergeDialog(wx.Dialog):
	def __init__(self, mainFrame):
		wx.Dialog.__init__(self, mainFrame)
		self.SetTitle("Merge...")
		self.mainFrame = mainFrame
		
		try:
			branches = mainFrame.git("for-each-ref", "refs/heads", "--format=%(refname:short)").splitlines()
		except Exception as e:
			branches = []
			
		masterSizer = wx.BoxSizer(wx.VERTICAL)
		
		lbl = wx.StaticText(self, label="Enter ref to merge into the current branch:")
		masterSizer.Add(lbl, 0, wx.ALL | wx.EXPAND, 10)
		
		self.chBranch = wx.ComboBox(self)
		masterSizer.Add(self.chBranch, 0, (wx.ALL ^ wx.TOP) | wx.EXPAND, 10)
		
		self.chBranch.Append(branches)
		try:
			symref = mainFrame.git("symbolic-ref", "HEAD")
			self.chBranch.SetValue(symref.split("\n")[0].split("/")[-1])
		except:
			pass
			
		self.cbNoFF = wx.CheckBox(self, label="Force new commit (no fast-forward)")
		masterSizer.Add(self.cbNoFF, 0, (wx.ALL ^ wx.TOP) | wx.EXPAND, 10)

		self.cbSquash = wx.CheckBox(self, label="Squash all changes into a single commit")
		masterSizer.Add(self.cbSquash, 0, (wx.ALL ^ wx.TOP) | wx.EXPAND, 10)
		
		btnSizer = self.CreateSeparatedButtonSizer(wx.OK | wx.CANCEL)
		masterSizer.Add(btnSizer, 0, (wx.ALL ^ wx.TOP) | wx.EXPAND, 10)
		
		masterSizer.SetSizeHints(self)
		self.SetSizer(masterSizer)
		
		self.Bind(wx.EVT_BUTTON, self.onOK, id = wx.ID_OK)
		self.Bind(wx.EVT_CHECKBOX, self.onCheckbox)
	
	def onCheckbox(self, e):
		if self.cbNoFF.GetValue():
			self.cbSquash.Disable()
		elif self.cbSquash.GetValue():
			self.cbNoFF.Disable()
		else:
			self.cbSquash.Enable()
			self.cbNoFF.Enable()
		
	def onOK(self, e):
		branchName = self.chBranch.GetValue()
		if branchName == "" or branchName.count("/") != 0:
			wx.MessageBox("Invalid branch name", "Madd Studio", wx.ICON_ERROR)
		else:
			self.EndModal(wx.ID_OK)
	
	def getBranchName(self):
		return self.chBranch.GetValue()
	
	def shouldDisableFF(self):
		return self.cbNoFF.GetValue()
	
	def shouldSquash(self):
		return self.cbSquash.GetValue()