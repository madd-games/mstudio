"""
	Madd Studio

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

from mstudio.UserConfig import *
import wx

"""The 'build configurations' dialog.

Guess what it does.
"""
class BuildConfigs(wx.Dialog):
	def __init__(self, parent, project):
		wx.Dialog.__init__(self, parent)
		self.SetTitle("%s - Build configurations" % project.name)
		self.SetMinSize(wx.Size(-1, 400))
		
		self.parent = parent
		self.project = project
		
		topSizer = wx.BoxSizer(wx.VERTICAL)
		
		horSizer = wx.BoxSizer(wx.HORIZONTAL)
		topSizer.Add(horSizer, 1, wx.ALL|wx.EXPAND, 10)
		
		configSizer = wx.BoxSizer(wx.VERTICAL)
		horSizer.Add(configSizer, 1, wx.ALL|wx.EXPAND, 2)
		
		self.lstConfigs = wx.ListBox(self, style=wx.LB_SORT)
		configSizer.Add(self.lstConfigs, 1, wx.ALL|wx.EXPAND, 0)
		
		configButtonSizer = wx.BoxSizer(wx.HORIZONTAL)
		configSizer.Add(configButtonSizer, 0, wx.ALL|wx.ALIGN_LEFT, 2)
		
		self.btnAdd = wx.Button(self, wx.ID_ADD)
		configButtonSizer.Add(self.btnAdd, 0, wx.RIGHT, 2)
		
		self.btnRemove = wx.Button(self, wx.ID_REMOVE)
		configButtonSizer.Add(self.btnRemove, 0, 0, 0)
		self.btnRemove.Disable()
		
		keys = project.info["BuildConfigs"].keys()
		if len(keys) != 0:
			self.lstConfigs.Append(list(keys))
		
		propSizer = wx.BoxSizer(wx.VERTICAL)
		horSizer.Add(propSizer, 2, wx.ALL|wx.ALIGN_TOP, 2)
		
		propSizer.Add(wx.StaticText(self, -1, "Configuration name:"), 0, wx.ALL|wx.EXPAND, 2)
		self.ctrlConfigName = wx.TextCtrl(self)
		self.ctrlConfigName.Disable()
		propSizer.Add(self.ctrlConfigName, 0, wx.ALL|wx.EXPAND, 2)

		propSizer.Add(wx.StaticText(self, -1, "Build directory:"), 0, wx.ALL|wx.EXPAND, 2)
		self.ctrlDir = wx.DirPickerCtrl(self)
		self.ctrlDir.Disable()
		propSizer.Add(self.ctrlDir, 0, wx.ALL|wx.EXPAND, 2)

		propSizer.Add(wx.StaticText(self, -1, "Arguments for 'configure':"), 0, wx.ALL|wx.EXPAND, 2)
		self.ctrlConfigArgs = wx.TextCtrl(self)
		self.ctrlConfigArgs.Disable()
		propSizer.Add(self.ctrlConfigArgs, 0, wx.ALL|wx.EXPAND, 2)

		topSizer.Add(self.CreateSeparatedButtonSizer(wx.CLOSE), 0, wx.ALL|wx.EXPAND, 10)

		self.SetSizer(topSizer)
		topSizer.SetSizeHints(self)
		
		self.Bind(wx.EVT_BUTTON, self.onAdd, id = wx.ID_ADD)
		self.Bind(wx.EVT_BUTTON, self.onRemove, id = wx.ID_REMOVE)
		self.Bind(wx.EVT_LISTBOX, self.onSelect)
		self.Bind(wx.EVT_TEXT, self.onTextChanged)
		self.Bind(wx.EVT_DIRPICKER_CHANGED, self.onDirChanged)
		self.Bind(wx.EVT_BUTTON, self.onClose, id = wx.ID_CLOSE)
	
	def onClose(self, e):
		self.parent.activeProject = None
		self.parent.setActiveProject(self.project)
		self.EndModal(wx.CLOSE)
		
	def onDirChanged(self, e):
		i = self.lstConfigs.GetSelection()
		name = self.lstConfigs.GetString(i)
		self.project.info["BuildConfigs"][name]["Dir"] = self.ctrlDir.GetPath()
		SaveWorkspace()
		
	def onTextChanged(self, e):
		i = self.lstConfigs.GetSelection()
		name = self.lstConfigs.GetString(i)
		
		if e.GetEventObject() == self.ctrlConfigName:
			newName = self.ctrlConfigName.GetValue()
			if name != newName:
				if newName not in self.project.info["BuildConfigs"]:
					self.project.info["BuildConfigs"][newName] = self.project.info["BuildConfigs"].pop(name)
					self.lstConfigs.SetString(i, newName)
					SaveWorkspace()
		elif e.GetEventObject() == self.ctrlConfigArgs:
			self.project.info["BuildConfigs"][name]["ConfigArgs"] = self.ctrlConfigArgs.GetValue()
			SaveWorkspace()
		
	def onRemove(self, e):
		i = self.lstConfigs.GetSelection()
		name = self.lstConfigs.GetString(i)
		del self.project.info["BuildConfigs"][name]
		self.lstConfigs.Delete(i)
		SaveWorkspace()
		self.onSelect(e)
		
	def makeLabel(self, i):
		return "Config_%d" % i
	
	def onSelect(self, e):
		i = self.lstConfigs.GetSelection()
		if i == -1:
			self.btnRemove.Disable()
			self.ctrlConfigName.Disable()
			self.ctrlDir.Disable()
			self.ctrlConfigArgs.Disable()
			self.ctrlConfigName.SetValue("")
			self.ctrlDir.SetPath("")
			self.ctrlConfigArgs.SetValue("")
		else:
			name = self.lstConfigs.GetString(i)
			self.btnRemove.Enable()
			self.ctrlConfigName.Enable()
			self.ctrlDir.Enable()
			self.ctrlConfigArgs.Enable()
			self.ctrlConfigName.SetValue(name)
			self.ctrlDir.SetPath(self.project.info["BuildConfigs"][name]["Dir"])
			self.ctrlConfigArgs.SetValue(self.project.info["BuildConfigs"][name]["ConfigArgs"])
		
	def onAdd(self, e):
		i = 1
		while self.makeLabel(i) in self.project.info["BuildConfigs"]:
			i += 1
		
		lbl = self.makeLabel(i)
		self.project.info["BuildConfigs"][lbl] = {
			"Dir": "",
			"ConfigArgs": ""
		}
		
		self.lstConfigs.Append(lbl)
		SaveWorkspace()
