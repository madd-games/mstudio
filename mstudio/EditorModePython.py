"""
	Madd Studio

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

from mstudio.EditorMode import *
import re

"""Editor mode for Python files.

"""
class EditorModePython(EditorMode):
	def __init__(self):
		self.stateTable = [
			[
				# keywords
				(re.compile(r'class'), "keyword", 0),
				(re.compile(r'finally'), "keyword", 0),
				(re.compile(r'is'), "keyword", 0),
				(re.compile(r'return'), "keyword", 0),
				(re.compile(r'continue'), "keyword", 0),
				(re.compile(r'for'), "keyword", 0),
				(re.compile(r'lambda'), "keyword", 0),
				(re.compile(r'try'), "keyword", 0),
				(re.compile(r'def'), "keyword", 0),
				(re.compile(r'from'), "keyword", 0),
				(re.compile(r'nonlocal'), "keyword", 0),
				(re.compile(r'while'), "keyword", 0),
				(re.compile(r'and'), "keyword", 0),
				(re.compile(r'del'), "keyword", 0),
				(re.compile(r'global'), "keyword", 0),
				(re.compile(r'not'), "keyword", 0),
				(re.compile(r'with'), "keyword", 0),
				(re.compile(r'as'), "keyword", 0),
				(re.compile(r'elif'), "keyword", 0),
				(re.compile(r'if'), "keyword", 0),
				(re.compile(r'or'), "keyword", 0),
				(re.compile(r'yield'), "keyword", 0),
				(re.compile(r'assert'), "keyword", 0),
				(re.compile(r'else'), "keyword", 0),
				(re.compile(r'import'), "keyword", 0),
				(re.compile(r'pass'), "keyword", 0),
				(re.compile(r'break'), "keyword", 0),
				(re.compile(r'except'), "keyword", 0),
				(re.compile(r'in'), "keyword", 0),
				(re.compile(r'raise'), "keyword", 0),
				
				# "built-ins"
				(re.compile(r'True'), "userType", 0),
				(re.compile(r'False'), "userType", 0),
				(re.compile(r'None'), "userType", 0),
				(re.compile(r'print'), "userType", 0),
				
				# everything that isn't a keyword
				(re.compile(r'\s+'), None, 0),					# whitespace
				(re.compile(r'\#[\w\W]*?(\n|$)'), "comment", 0),		# single-line comment with '#'
				(re.compile(r'[_a-zA-Z][_a-zA-Z0-9]*'), None, 0),		# identifier
				(re.compile(r'[ru]?"(\\.|.)*?("|$|\n)'), "string", 0),		# double-quote string
				(re.compile(r'[ru]?\'(\\.|.)*?(\'|$|\n)'), "string", 0),	# single-quote string
				(re.compile(r'[ru]?\'\'\'[\w\W]*?(\'\'\'|$)'), "string", 0),	# multi-line single-quote string
				(re.compile(r'[ru]?"""[\w\W]*?("""|$)'), "string", 0),		# multi-line double-quote string
			]
		]

	def isForFile(self, filename, content):
		if filename.endswith(".py") or filename.endswith(".pyw"):
			return True
		else:
			return False

	def getContextualNewline(self, before, after):
		lastLine = before.split("\n")[-1]
		tabDepth = len(lastLine) - len(lastLine.lstrip("\t"))
		if before.endswith(":"):
			tabDepth += 1
		return ("\t" * tabDepth, "")

	def getIconName(self, filename, content):
		return "Python"
