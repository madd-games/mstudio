"""
	Madd Studio

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

from mstudio.Navigator import Navigator
from mstudio.CodeEditor import CodeEditor
from mstudio.EditorFolder import EditorFolder
from mstudio.EditorFile import EditorFile
from mstudio.NewProjectDialog import NewProjectDialog
from mstudio.UserConfig import *
from mstudio.Project import Project
from mstudio.FindDialog import FindDialog
from mstudio.Preferences import Preferences
from mstudio.BuildConfigs import BuildConfigs
from mstudio.RunConfigs import RunConfigs
from mstudio.BuildPanel import BuildPanel
from mstudio.EditorModeList import editorModeList
from mstudio.Plugin import *
from mstudio.GitStagingDialog import GitStagingDialog
from mstudio.GitRemotesDialog import GitRemotesDialog
from mstudio.GitRemoteBranchPicker import GitRemoteBranchPicker
from mstudio.GitCheckoutDialog import GitCheckoutDialog
from mstudio.GitMergeDialog import GitMergeDialog
from mstudio.MBSMetaParser import MBSMetaParser
from subprocess import Popen, PIPE
from functools import partial
import mstudio.Terminal as Terminal
import wx
import sys, os

"""The main frame.

This is the main IDE window, with the navigator and code editor etc.
"""
class MainFrame(wx.Frame):
	def __init__(self):
		wx.Frame.__init__(self, None, title="Madd Studio")
		self.splitter = wx.SplitterWindow(self)
		self.SetIcon(wx.Icon(os.path.dirname(__file__) + "/../data/images/mstudio-large.png"))
		
		self.navigator = Navigator(self.splitter, self)
		self.codeSplitter = wx.SplitterWindow(self.splitter)
		self.editor = CodeEditor(self.codeSplitter, self)
		self.buildPanel = BuildPanel(self.codeSplitter, self)
		
		self.splitter.SplitVertically(self.navigator, self.codeSplitter)
		self.splitter.SetMinimumPaneSize(200)
		self.splitter.SetSashPosition(userConfig["MainFrame.NavigatorSash"], True)

		self.codeSplitter.SplitHorizontally(self.editor, self.buildPanel)
		self.codeSplitter.SetMinimumPaneSize(20)
		self.codeSplitter.SetSashPosition(userConfig["MainFrame.CodeSash"], True)

		self.navigator.setEditor(self.editor)
		
		self.findDialog = None
		
		self.ID_SAVE_ALL = self.NewControlId()
		self.ID_NEW_FOLDER = self.NewControlId()
		self.ID_NEW_FILE = self.NewControlId()
		self.ID_NEW_PROJECT = self.NewControlId()
		self.ID_DELETE_FILE = self.NewControlId()
		self.ID_REMOVE_PROJECT = self.NewControlId()
		self.ID_BUILD_CONFIGS = self.NewControlId()
		self.ID_RUN_CONFIGS = self.NewControlId()
		self.ID_BUILD = self.NewControlId()
		self.ID_BUILD_CONSOLE = self.NewControlId()
		self.ID_BOOKMARK = self.NewControlId()
		self.ID_GIT_INIT = self.NewControlId()
		self.ID_GIT_STAGING = self.NewControlId()
		self.ID_GIT_REMOTES = self.NewControlId()
		self.ID_GIT_FETCH = self.NewControlId()
		self.ID_GIT_PULL = self.NewControlId()
		self.ID_GIT_PUSH = self.NewControlId()
		self.ID_GIT_STASH = self.NewControlId()
		self.ID_GIT_POP_STASH = self.NewControlId()
		self.ID_GIT_CHECKOUT = self.NewControlId()
		self.ID_GIT_MERGE = self.NewControlId()
		self.ID_GIT_MERGE_ABORT = self.NewControlId()
		self.ID_DEBUG_SEMANTIC_HIGHLIGHT = self.NewControlId()
		
		newmenu = wx.Menu()
		newmenu.Append(self.ID_NEW_PROJECT, "Project...", "Create a new project in the current workspace directory.")
		newmenu.AppendSeparator()
		newmenu.Append(self.ID_NEW_FOLDER, "Folder...", "Create a new folder inside the selected folder.")
		newmenu.Append(self.ID_NEW_FILE, "File...", "Create a new file inside the selected folder.")

		filemenu = wx.Menu()
		filemenu.AppendSubMenu(newmenu, "New...")
		filemenu.AppendSeparator()
		filemenu.Append(self.ID_DELETE_FILE, "Delete...", "Delete the selected file or folder.")
		filemenu.Append(self.ID_REMOVE_PROJECT, "Remove project", "Remove the selected project from the workspace.")
		filemenu.AppendSeparator()
		filemenu.Append(wx.ID_SAVE)
		filemenu.Append(self.ID_SAVE_ALL, "Save all", "Save all files which have been modified.")
		filemenu.AppendSeparator()
		filemenu.Append(wx.ID_EXIT, "E&xit", "Terminate the program")
		
		modemenu = wx.Menu()
		modeNames = list(editorModeList.keys())
		modeNames.sort()
		for name in modeNames:
			item = modemenu.Append(-1, name, "Set the file editor mode")
			self.Bind(wx.EVT_MENU, partial(self.onSetMode, name), item)
		
		editmenu = wx.Menu()
		editmenu.Append(wx.ID_FIND)
		editmenu.AppendSeparator()
		editmenu.AppendSubMenu(modemenu, "Set file mode...")
		editmenu.AppendSeparator()
		editmenu.Append(wx.ID_PREFERENCES)
		
		buildmenu = wx.Menu()
		buildmenu.Append(self.ID_BUILD_CONFIGS, "Build configurations...", "Set up profiles of build settings for this project.")
		buildmenu.Append(self.ID_RUN_CONFIGS, "Run configurations...", "Set up scripts to run and/or test builds.")
		buildmenu.AppendSeparator()
		buildmenu.Append(self.ID_BUILD, "Build", "Build using the currently selected configuration.")
		buildmenu.Append(self.ID_BUILD_CONSOLE, "Build console...", "Open a terminal with the build environment set up.")
		
		gitmenu = wx.Menu()
		self.itemGitInit = gitmenu.Append(self.ID_GIT_INIT, "Initialize...", "Make a git repository for this project.")
		self.itemGitInit.Enable(False)
		gitmenu.AppendSeparator()
		self.itemGitStaging = gitmenu.Append(self.ID_GIT_STAGING, "Staging...", "Show a dialog with changes and commit options.")
		self.itemGitStaging.Enable(False)
		self.itemGitRemotes = gitmenu.Append(self.ID_GIT_REMOTES, "Remotes...", "Manage remote repositories")
		self.itemGitRemotes.Enable(False)
		gitmenu.AppendSeparator()
		self.itemGitFetch = gitmenu.Append(self.ID_GIT_FETCH, "Fetch...", "Fetch data from the remote repository")
		self.itemGitFetch.Enable(False)
		self.itemGitPull = gitmenu.Append(self.ID_GIT_PULL, "Pull...", "Pull from a remote branch")
		self.itemGitPull.Enable(False)
		self.itemGitPush = gitmenu.Append(self.ID_GIT_PUSH, "Push...", "Push to a remote branch")
		self.itemGitPush.Enable(False)
		gitmenu.AppendSeparator()
		self.itemGitStash = gitmenu.Append(self.ID_GIT_STASH, "Stash...", "Stash any uncomitted changes")
		self.itemGitStash.Enable(False)
		self.itemGitPopStash = gitmenu.Append(self.ID_GIT_POP_STASH, "Pop the stash...", "Restore stashed changes")
		self.itemGitStash.Enable(False)
		gitmenu.AppendSeparator()
		self.itemGitCheckout = gitmenu.Append(self.ID_GIT_CHECKOUT, "Checkout...", "Check out a branch")
		self.itemGitCheckout.Enable(False)
		self.itemGitMerge = gitmenu.Append(self.ID_GIT_MERGE, "Merge...", "Merge a branch into current branch")
		self.itemGitMerge.Enable(False)
		self.itemGitMergeAbort = gitmenu.Append(self.ID_GIT_MERGE_ABORT, "Abort merge", "Abort the current merge (if any)")
		self.itemGitMergeAbort.Enable(False)
		
		menuBar = wx.MenuBar()
		menuBar.Append(filemenu, "&File")
		menuBar.Append(editmenu, "&Edit")
		menuBar.Append(buildmenu, "&Build")
		menuBar.Append(gitmenu, "Git")
		PluginInitMenuBar(self, menuBar)
		
		if userConfig["Debug.Enable"]:
			debugmenu = wx.Menu()
			menuBar.Append(debugmenu, "Debug")
			
			debugmenu.Append(self.ID_DEBUG_SEMANTIC_HIGHLIGHT, "Semantic highlight...", "Manually trigger the semantic highlighter")
		
		self.SetMenuBar(menuBar)
		
		toolBar = self.CreateToolBar()
		toolBar.AddTool(self.ID_NEW_FOLDER, "New folder",
			wx.Bitmap(os.path.dirname(__file__) + "/../data/toolbar/newdir.png"),
			shortHelp = "Create a new subfolder in the current folder")
		toolBar.AddTool(self.ID_NEW_FILE, "New file",
			wx.Bitmap(os.path.dirname(__file__) + "/../data/toolbar/newfile.png"),
			shortHelp = "Create a new file in the current folder")
		toolBar.AddSeparator()
		toolBar.AddTool(wx.ID_SAVE, "Save",
			wx.Bitmap(os.path.dirname(__file__) + "/../data/toolbar/save.png"),
			shortHelp = "Save the current file")
		toolBar.AddTool(self.ID_SAVE_ALL, "Save all",
			wx.Bitmap(os.path.dirname(__file__) + "/../data/toolbar/saveall.png"),
			shortHelp = "Save all visible files")
		toolBar.AddSeparator()
		toolBar.AddTool(self.ID_BOOKMARK, "Bookmark", wx.ArtProvider.GetBitmap(wx.ART_ADD_BOOKMARK, wx.ART_TOOLBAR), shortHelp = "Set bookmark (background color for file in navigator)")
		toolBar.AddSeparator()
		
		toolBar.AddControl(wx.StaticText(toolBar, -1, "Build config:"))
		self.buildChoice = wx.Choice(toolBar)
		toolBar.AddControl(self.buildChoice)
		self.btnBuild = toolBar.AddTool(self.ID_BUILD, "Build",
					wx.Bitmap(os.path.dirname(__file__) + "/../data/toolbar/build.png"),
					shortHelp = "Run the specified build")
		self.btnBuildConsole = toolBar.AddTool(self.ID_BUILD_CONSOLE, "Build console",
					wx.Bitmap(os.path.dirname(__file__) + "/../data/toolbar/console.png"),
					shortHelp = "Open the build console")
		
		self.buildChoice.Disable()
		
		self.activeProject = None
		
		toolBar.Realize()
		
		self.statusBar = self.CreateStatusBar(2)
		self.statusBar.SetStatusWidths([-1, 200])
		
		self.Fit()
		self.Show(True)
		self.Maximize(True)

		self.Bind(wx.EVT_MENU, self.onSave, id = wx.ID_SAVE)
		self.Bind(wx.EVT_CLOSE, self.onClose)
		self.Bind(wx.EVT_MENU, self.onSaveAll, id = self.ID_SAVE_ALL)
		self.Bind(wx.EVT_MENU, self.onNewFolder, id = self.ID_NEW_FOLDER)
		self.Bind(wx.EVT_MENU, self.onNewFile, id = self.ID_NEW_FILE)
		self.Bind(wx.EVT_MENU, self.onNewProject, id = self.ID_NEW_PROJECT)
		self.Bind(wx.EVT_MENU, self.onDelete, id = self.ID_DELETE_FILE)
		self.Bind(wx.EVT_MENU, self.onRemoveProject, id = self.ID_REMOVE_PROJECT)
		self.Bind(wx.EVT_MENU, self.onFind, id = wx.ID_FIND)
		self.Bind(wx.EVT_MENU, self.onPreferences, id = wx.ID_PREFERENCES)
		self.Bind(wx.EVT_MENU, self.onBuildConfigs, id = self.ID_BUILD_CONFIGS)
		self.Bind(wx.EVT_MENU, self.onRunConfigs, id = self.ID_RUN_CONFIGS)
		self.Bind(wx.EVT_SPLITTER_SASH_POS_CHANGED, self.onSashChanged)
		self.Bind(wx.EVT_MENU, self.onBuild, id = self.ID_BUILD)
		self.Bind(wx.EVT_MENU, self.onBuildConsole, id = self.ID_BUILD_CONSOLE)
		self.Bind(wx.EVT_MENU, self.onBookmark, id = self.ID_BOOKMARK)
		self.Bind(wx.EVT_MENU, self.onGitInit, id = self.ID_GIT_INIT)
		self.Bind(wx.EVT_MENU, self.onGitStaging, id = self.ID_GIT_STAGING)
		self.Bind(wx.EVT_MENU, self.onGitRemotes, id = self.ID_GIT_REMOTES)
		self.Bind(wx.EVT_MENU, self.onGitFetch, id = self.ID_GIT_FETCH)
		self.Bind(wx.EVT_MENU, self.onGitPull, id = self.ID_GIT_PULL)
		self.Bind(wx.EVT_MENU, self.onGitPush, id = self.ID_GIT_PUSH)
		self.Bind(wx.EVT_MENU, self.onGitStash, id = self.ID_GIT_STASH)
		self.Bind(wx.EVT_MENU, self.onGitPopStash, id = self.ID_GIT_POP_STASH)
		self.Bind(wx.EVT_MENU, self.onGitCheckout, id = self.ID_GIT_CHECKOUT)
		self.Bind(wx.EVT_MENU, self.onGitMerge, id = self.ID_GIT_MERGE)
		self.Bind(wx.EVT_MENU, self.onGitMergeAbort, id = self.ID_GIT_MERGE_ABORT)
		self.Bind(wx.EVT_MENU, self.onSemanticHighlight, id = self.ID_DEBUG_SEMANTIC_HIGHLIGHT)
		self.Bind(wx.EVT_CHOICE, self.onChoice)
		
		wx.CallLater(1500, self.onTick)
	
	def onTick(self):
		wx.CallLater(1500, self.onTick)
		self.updateGitInfo()
	
	def onSemanticHighlight(self, e):
		if self.activeProject is not None:
			if len(self.activeProject.info["BuildConfigs"]) != 0:
				name = self.buildChoice.GetString(self.buildChoice.GetSelection())
				buildDir = self.activeProject.info["BuildConfigs"][name]["Dir"]
				self.editor.beginSemanticHighlight(buildDir)

	def updateGitInfo(self):
		self.statusBar.SetStatusText("Branch: " + self.getCurrentBranchName(), 1)
		
		activeProject = self.getActiveProject()
		if activeProject is None:
			self.itemGitInit.Enable(False)
			self.itemGitStaging.Enable(False)
			self.itemGitRemotes.Enable(False)
			self.itemGitFetch.Enable(False)
			self.itemGitPull.Enable(False)
			self.itemGitPush.Enable(False)
			self.itemGitStash.Enable(False)
			self.itemGitPopStash.Enable(False)
			self.itemGitCheckout.Enable(False)
			self.itemGitMerge.Enable(False)
			self.itemGitMergeAbort.Enable(False)
		else:
			root = activeProject.getRootDir()
			gitdir = os.path.join(root, ".git")
			
			if os.path.isdir(gitdir):
				# There is a repository
				self.itemGitInit.Enable(False)
				self.itemGitStaging.Enable(True)
				self.itemGitRemotes.Enable(True)
				self.itemGitFetch.Enable(True)
				self.itemGitPull.Enable(True)
				self.itemGitPush.Enable(True)
				self.itemGitStash.Enable(True)
				self.itemGitPopStash.Enable(True)
				self.itemGitCheckout.Enable(True)
				self.itemGitMerge.Enable(True)
				self.itemGitMergeAbort.Enable(True)
			else:
				# There is no repository
				self.itemGitInit.Enable(True)
				self.itemGitStaging.Enable(False)
				self.itemGitRemotes.Enable(False)
				self.itemGitFetch.Enable(False)
				self.itemGitPull.Enable(False)
				self.itemGitPush.Enable(False)
				self.itemGitStash.Enable(False)
				self.itemGitPopStash.Enable(False)
				self.itemGitCheckout.Enable(False)
				self.itemGitMerge.Enable(False)
				self.itemGitMergeAbort.Enable(False)

	def onGitStaging(self, e):
		dlg = GitStagingDialog(self)
		dlg.ShowModal()
		dlg.Destroy()
		self.updateGitInfo()
	
	def onGitRemotes(self, e):
		remotes = {}
		try:
			data = self.git("remote", "-v")
			lines = data.splitlines()
			for line in lines:
				if "\t" in line:
					name, info = line.split("\t")
					remotes[name] = info.split(' ')[0]
		except Exception as e:
			wx.MessageBox(str(e), "Madd Studio", wx.ICON_ERROR)
			
		dlg = GitRemotesDialog(self, remotes)
		dlg.ShowModal()
		dlg.Destroy()
		self.updateGitInfo()
	
	def onGitFetch(self, e):
		self.detachedGit("fetch")
		
	def gitPullPush(self, which):
		prompt = "Select remote branch to push to:" if which == "push" else "Select remote branch to pull from:"
		dlg = GitRemoteBranchPicker(self, prompt)
		if dlg.ShowModal() != wx.ID_OK:
			return
		
		cmd = [which]
		if dlg.shouldSetUpstream():
			cmd.append("--set-upstream")
		
		cmd.extend(dlg.getBranchName().split("/"))
		dlg.Destroy()
		
		self.detachedGit(*cmd)
		
	def onGitPull(self, e):
		self.gitPullPush("pull")
	
	def onGitPush(self, e):
		self.gitPullPush("push")
	
	def onGitCheckout(self, e):
		dlg = GitCheckoutDialog(self)
		if dlg.ShowModal() != wx.ID_OK:
			return
		
		cmd = ["checkout"]
		if dlg.shouldMakeNew():
			cmd.append("-b")
		cmd.append(dlg.getBranchName())
		
		dlg.Destroy()
		self.detachedGit(*cmd)
	
	def onGitMerge(self, e):
		dlg = GitMergeDialog(self)
		if dlg.ShowModal() != wx.ID_OK:
			return
		
		cmd = ["merge", dlg.getBranchName()]
		if dlg.shouldDisableFF(): cmd.append("--no-ff")
		if dlg.shouldSquash(): cmd.append("--squash")
		
		dlg.Destroy()
		self.detachedGit(*cmd)
	
	def onGitMergeAbort(self, e):
		if wx.MessageBox("Are you sure you want to abort the current merge?", "Madd Studio", wx.ICON_WARNING | wx.YES | wx.NO) == wx.YES:
			self.detachedGit("merge", "--abort")
	
	def onGitStash(self, e):
		self.detachedGit("stash")
	
	def onGitPopStash(self, e):
		self.detachedGit("stash", "pop")

	def onGitInit(self, e):
		try:
			msg = self.git("init")
			wx.MessageBox(msg, "Madd Studio", wx.ICON_INFORMATION)
		except Exception as e:
			wx.MessageBox(str(e), "Madd Studio", wx.ICON_ERROR)
		
		self.updateGitInfo()
		
	def onSetMode(self, name, e):
		self.editor.setModeManually(name)
		
	def onBookmark(self, e):
		if self.editor.file is not None:
			item = self.editor.file.item
			data = wx.ColourData()
			data.SetColour(self.navigator.GetItemBackgroundColour(item))
			dlg = wx.ColourDialog(self, data)
			if dlg.ShowModal() == wx.ID_OK:
				self.navigator.SetItemBackgroundColour(item, dlg.GetColourData().GetColour())
				dlg.Destroy()
	
	def getActiveProject(self):
		return self.activeProject
		
	def getActiveBuildConfigName(self):
		if len(self.activeProject.info["BuildConfigs"]) != 0:
			return self.buildChoice.GetString(self.buildChoice.GetSelection())
		else:
			return None
		
	def onBuild(self, e):
		self.onSaveAll(e)
		if self.activeProject is not None:
			if len(self.activeProject.info["BuildConfigs"]) != 0:
				if not self.buildPanel.buildRunning:
					name = self.buildChoice.GetString(self.buildChoice.GetSelection())
					self.buildPanel.runBuild(self.activeProject, name)
	
	def onBuildConsole(self, e):
		if self.activeProject is not None and len(self.activeProject.info["BuildConfigs"]) != 0:
			configName = self.buildChoice.GetString(self.buildChoice.GetSelection())
			dirname = self.activeProject.info["BuildConfigs"][configName]["Dir"]
			
			env = os.environ.copy()
			packages = []
			prefixDecl = []
			
			metaParser = MBSMetaParser()
			metaParser.on("mbs.prefix", prefixDecl.append)
			metaParser.on("mbs.pkg", packages.append)
			metaParser.feed(dirname + "/mbs.meta")
			
			prefix = "/usr"
			if len(prefixDecl) != 0:
				prefix = prefixDecl[0]
			
			paths = [env["PATH"]]
			libPaths = []
			if "LD_LIBRARY_PATH" in env:
				libPaths.append(env["LD_LIBRARY_PATH"])
			
			for pkg in packages:
				paths.insert(0, dirname + "/" + pkg + prefix + "/bin")
				libPaths.insert(0, dirname + "/" + pkg + prefix + "/lib")
			
			env["PATH"] = ":".join(paths)
			env["LD_LIBRARY_PATH"] = ":".join(libPaths)
			
			ps1 = userConfig["Build.ConsolePrompt"]
			ps1 = ps1.replace("${PROJECT_NAME}", self.activeProject.name)
			ps1 = ps1.replace("${BUILD_CONFIG}", configName)
			env["PS1"] = ps1
			
			Terminal.openBuildConsole(cwd=dirname, env=env)
		else:
			wx.MessageBox("Please select a project and a build configuration to open the build console.", "Madd Studio", wx.ICON_ERROR)
				
	def onChoice(self, e):
		if self.activeProject is not None:
			self.activeProject.info["SelectedBuildConfig"] = self.buildChoice.GetSelection()
			SaveWorkspace()
			
	def setActiveProject(self, project):
		if self.activeProject != project:
			self.activeProject = project
			
			self.buildChoice.Clear()
			if len(project.info["BuildConfigs"]) == 0:
				self.buildChoice.Disable()
			else:
				self.buildChoice.Enable()
				for key in project.info["BuildConfigs"].keys():
					self.buildChoice.Append(key)
				try:
					self.buildChoice.SetSelection(project.info["SelectedBuildConfig"])
				except:
					self.buildChoice.SetSelection(0)
			
			self.updateGitInfo()
			
	def onSashChanged(self, e):
		userConfig["MainFrame.NavigatorSash"] = self.splitter.GetSashPosition()
		userConfig["MainFrame.CodeSash"] = self.codeSplitter.GetSashPosition()
		SaveWorkspace()
		
	def onBuildConfigs(self, e):
		proj = self.activeProject
		if proj is not None:
			dlg = BuildConfigs(self, proj)
			dlg.ShowModal()
			dlg.Destroy()
	
	def onRunConfigs(self, e):
		proj = self.activeProject
		if proj is not None:
			dlg = RunConfigs(self, proj)
			dlg.ShowModal()
			dlg.Destroy()
			
	def onPreferences(self, e):
		dlg = Preferences(self)
		dlg.ShowModal()
		dlg.Destroy()
		
	def onFind(self, e):
		if self.findDialog is None:
			dlg = FindDialog(self, self.editor)
			dlg.Show(True)
			self.findDialog = dlg
		else:
			self.findDialog.SetFocus()
		
	def onDelete(self, e):
		self.navigator.delete()
	
	def onRemoveProject(self, e):
		self.navigator.removeProject()
		
	def onNewProject(self, e):
		dlg = NewProjectDialog(self)
		if dlg.ShowModal() == wx.ID_CANCEL:
			return
		
		name = dlg.getProjectName()
		dirname = dlg.getProjectDir()
		callback = dlg.getInitCallback()
		dlg.Destroy()
		
		info = {
			"ProjectDir": dirname,
			"BuildConfigs": {},
			"RunConfigs": {}
		}
		
		userConfig['Projects'][name] = info
		if callback is not None:
			callback(dirname)
		self.navigator.projects.append(Project(self.navigator, name, info))
		SaveWorkspace()
	
	def onNewFolder(self, e):
		self.navigator.newFolder()
	
	def onNewFile(self, e):
		self.navigator.newFile()
		
	def onSaveAll(self, e):
		self.navigator.saveAll()
		
	def onSave(self, e):
		if self.editor.file is not None:
			self.editor.file.save()

	def onClose(self, e):
		if not self.navigator.hasDirty():
			e.Skip()
			return
		
		answer = wx.MessageBox("Not all changes have been saved. Would you like to save before quitting?", "Madd Studio", wx.YES_NO | wx.CANCEL | wx.ICON_WARNING)
		if answer == wx.YES:
			if self.navigator.saveAll():
				e.Skip()
			return
		elif answer == wx.NO:
			e.Skip()
			return
		else:
			return

	"""Invoke git.
	
	`git` will be run in the root directory of the project, with the specified arguments, and return git's standard
	output. On error, an exception is raised.
	"""
	def git(self, *args, **kwargs):
		proj = self.getActiveProject()
		if proj is None:
			raise Exception("There is no active project")
		 
		pr = Popen(["git", *args], stdin=PIPE, stdout=PIPE, stderr=PIPE, cwd=proj.getRootDir())
		out, err = pr.communicate(kwargs.get("data", ""))
		out = out.decode("utf-8")
		err = err.decode("utf-8")
		print("git %s: stdout: %s" % (" ".join(args), out))
		print("git %s: stderr: %s" % (" ".join(args), err))
		if err != "":
			raise Exception(err)
		
		return out
	
	"""Invoke git in a terminal window."""
	def detachedGit(self, *args, **kwargs):
		proj = self.getActiveProject()
		if proj is None:
			raise Exception("There is no active project")
		
		Terminal.run("git", *args, cwd=proj.getRootDir())
		
	"""Get the current branch name."""
	def getCurrentBranchName(self):
		try:
			symref = self.git("symbolic-ref", "HEAD")
			return symref.split("\n")[0].split("/")[-1]
		except Exception as e:
			return "(none)"
