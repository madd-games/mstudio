"""
	Madd Studio

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

from mstudio.EditorMode import *
import re

"""Editor mode for web formats.

This includes HTML, (S)CSS, JavaScript, PHP, etc.
"""
class EditorModeWeb(EditorMode):
	def __init__(self, initState, iconName):
		EditorMode.__init__(self)
		self.initState = initState
		self.iconName = iconName
		
		self.stateTable = {
			"html":			[
				(re.compile(r'\<\?php'), "preprocessor", "php"),
				(re.compile(r'\<\!.*\>'), "preprocessor", "html"),
				(re.compile(r'\<'), "preprocessor", "html.tagname")
			],
			
			"html.tagname":		[
				(re.compile(r'[-0-9a-zA-Z]+'), "keyword", "html.props"),
				(re.compile(r'\/[0-9a-zA-Z]+'), "keyword", "html.props.forceEnd"),
				(re.compile(r'.'), "error", "html.error")
			],
			
			"html.error":		[
				(re.compile(r'.*'), "error", "html"),
			],
			
			"html.props":		[
				(re.compile(r'\s+'), "preprocessor", "html.props"),
				(re.compile(r'\>'), "preprocessor", "html"),
				(re.compile(r'[-0-9a-zA-Z]+'), "userType", "html.props.eq"),
				(re.compile(r'.'), "error", "html.error")
			],
			
			"html.props.forceEnd":	[
				(re.compile(r'\>'), "preprocessor", "html"),
				(re.compile(r'.'), "error", "html.error")
			],
			
			"html.props.eq":	[
				(re.compile(r'\='), "preprocessor", "html.props.value"),
				(re.compile(r'.'), "error", "html.error")
			],
			
			"html.props.value":	[
				(re.compile(r'"[^"]*"'), "string", "html.props"),
				(re.compile(r'.'), "error", "html.error")
			],
			
			"js":			[
				# keywords
				(re.compile("break"), "keyword", "js"),
				(re.compile("case"), "keyword", "js"),
				(re.compile("catch"), "keyword", "js"),
				(re.compile("continue"), "keyword", "js"),
				(re.compile("debugger"), "keyword", "js"),
				(re.compile("default"), "keyword", "js"),
				(re.compile("delete"), "keyword", "js"),
				(re.compile("do"), "keyword", "js"),
				(re.compile("else"), "keyword", "js"),
				(re.compile("finally"), "keyword", "js"),
				(re.compile("for"), "keyword", "js"),
				(re.compile("function"), "keyword", "js"),
				(re.compile("if"), "keyword", "js"),
				(re.compile("in"), "keyword", "js"),
				(re.compile("instanceof"), "keyword", "js"),
				(re.compile("new"), "keyword", "js"),
				(re.compile("return"), "keyword", "js"),
				(re.compile("switch"), "keyword", "js"),
				(re.compile("this"), "userType", "js"),
				(re.compile("throw"), "keyword", "js"),
				(re.compile("try"), "keyword", "js"),
				(re.compile("typeof"),  "userType", "js"),
				(re.compile("var"), "keyword", "js"),
				(re.compile("void"), "keyword", "js"),
				(re.compile("while"), "keyword", "js"),
				(re.compile("with"), "keyword", "js"),
				
				# built-ins
				(re.compile("null"), "userType", "js"),
				(re.compile("true"), "userType", "js"),
				(re.compile("false"), "userType", "js"),
				(re.compile("NaN"), "userType", "js"),
				(re.compile("Infinity"), "userType", "js"),
				(re.compile("undefined"), "userType", "js"),
				(re.compile("console"), "userType", "js"),
				(re.compile("Math"), "userType", "js"),
				
				# numbers
				(re.compile("(0x[0-9a-fA-F]+|0[0-7]*|[1-9][0-9]*)"), "number", "js"),
				(re.compile("(0x[0-9a-fA-F]+|0[0-7]*|[1-9][0-9]*)[uU]"), "number", "js"),
				(re.compile("(0x[0-9a-fA-F]+|0[0-7]*|[1-9][0-9]*)[lL]"), "number", "js"),
				(re.compile("(0x[0-9a-fA-F]+|0[0-7]*|[1-9][0-9]*)[uU][lL]"), "number", "js"),
				(re.compile("[0-9]*\\.[0-9]+([eE][-+][0-9]+)?"), "number", "js"),
				(re.compile("[0-9]*\\.[0-9]+([eE][-+][0-9]+)?[Ff]"), "number", "js"),

				# everything else
				(re.compile(r'\s+'), None, "js"),				# whitespace
				(re.compile(r'\/\/[\w\W]*?(\n|$)'), "comment", "js"),		# single-line comment
				(re.compile(r'\/\*[\w\W]*?(\*\/|$)'), "comment", "js"),		# block comment
				(re.compile(r'[_a-zA-Z][_a-zA-Z0-9]*'), None, "js"),		# identifier
				(re.compile(r'[ru]?"(\\.|.)*?("|$|\n)'), "string", "js"),	# double-quote string
				(re.compile(r'[ru]?\'(\\.|.)*?(\'|$|\n)'), "string", "js"),	# single-quote string
			],
			
			"php":		[
				# end of a PHP block
				(re.compile(r'\?\>'), "preprocessor", "html"),
				
				# keywords
				(re.compile("__halt_compiler"), "keyword", "php"),
				(re.compile("abstract"), "keyword", "php"),
				(re.compile("and"), "keyword", "php"),
				(re.compile("as"), "keyword", "php"),
				(re.compile("break"), "keyword", "php"),
				(re.compile("callable"), "keyword", "php"),
				(re.compile("case"), "keyword", "php"),
				(re.compile("catch"), "keyword", "php"),
				(re.compile("class"), "keyword", "php"),
				(re.compile("clone"), "keyword", "php"),
				(re.compile("const"), "keyword", "php"),
				(re.compile("continue"), "keyword", "php"),
				(re.compile("declare"), "keyword", "php"),
				(re.compile("default"), "keyword", "php"),
				(re.compile("do"), "keyword", "php"),
				(re.compile("echo"), "keyword", "php"),
				(re.compile("else"), "keyword", "php"),
				(re.compile("elseif"), "keyword", "php"),
				(re.compile("enddeclare"), "keyword", "php"),
				(re.compile("endfor"), "keyword", "php"),
				(re.compile("endforeach"), "keyword", "php"),
				(re.compile("endif"), "keyword", "php"),
				(re.compile("endswitch"), "keyword", "php"),
				(re.compile("endwhile"), "keyword", "php"),
				(re.compile("extends"), "keyword", "php"),
				(re.compile("final"), "keyword", "php"),
				(re.compile("finally"), "keyword", "php"),
				(re.compile("fn"), "keyword", "php"),
				(re.compile("for"), "keyword", "php"),
				(re.compile("foreach"), "keyword", "php"),
				(re.compile("from"), "keyword", "php"),
				(re.compile("function"), "keyword", "php"),
				(re.compile("global"), "keyword", "php"),
				(re.compile("goto"), "keyword", "php"),
				(re.compile("if"), "keyword", "php"),
				(re.compile("implements"), "keyword", "php"),
				(re.compile("include"), "keyword", "php"),
				(re.compile("include_once"), "keyword", "php"),
				(re.compile("instanceof"), "keyword", "php"),
				(re.compile("insteadof"), "keyword", "php"),
				(re.compile("interface"), "keyword", "php"),
				(re.compile("namespace"), "keyword", "php"),
				(re.compile("new"), "keyword", "php"),
				(re.compile("or"), "keyword", "php"),
				(re.compile("print"), "keyword", "php"),
				(re.compile("private"), "keyword", "php"),
				(re.compile("protected"), "keyword", "php"),
				(re.compile("public"), "keyword", "php"),
				(re.compile("require"), "keyword", "php"),
				(re.compile("require_once"), "keyword", "php"),
				(re.compile("return"), "keyword", "php"),
				(re.compile("static"), "keyword", "php"),
				(re.compile("switch"), "keyword", "php"),
				(re.compile("throw"), "keyword", "php"),
				(re.compile("trait"), "keyword", "php"),
				(re.compile("try"), "keyword", "php"),
				(re.compile("use"), "keyword", "php"),
				(re.compile("var"), "keyword", "php"),
				(re.compile("while"), "keyword", "php"),
				(re.compile("xor"), "keyword", "php"),
				(re.compile("yield"), "keyword", "php"),

				# compile-time constants
				(re.compile("__CLASS__"), "userType", "php"),
				(re.compile("__DIR__"), "userType", "php"),
				(re.compile("__FILE__"), "userType", "php"),
				(re.compile("__FUNCTION__"), "userType", "php"),
				(re.compile("__LINE__"), "userType", "php"),
				(re.compile("__METHOD__"), "userType", "php"),
				(re.compile("__NAMESPACE__"), "userType", "php"),
				(re.compile("__TRAIT__"), "userType", "php"),
				
				# builtins
				(re.compile("array"), "userType", "php"),
				(re.compile("die"), "userType", "php"),
				(re.compile("empty"), "userType", "php"),
				(re.compile("eval"), "userType", "php"),
				(re.compile("exit"), "userType", "php"),
				(re.compile("isset"), "userType", "php"),
				(re.compile("list"), "userType", "php"),
				(re.compile("unset"), "userType", "php"),
				
				# numbers
				(re.compile("(0x[0-9a-fA-F]+|0[0-7]*|[1-9][0-9]*)"), "number", "php"),
				(re.compile("(0x[0-9a-fA-F]+|0[0-7]*|[1-9][0-9]*)[uU]"), "number", "php"),
				(re.compile("(0x[0-9a-fA-F]+|0[0-7]*|[1-9][0-9]*)[lL]"), "number", "php"),
				(re.compile("(0x[0-9a-fA-F]+|0[0-7]*|[1-9][0-9]*)[uU][lL]"), "number", "php"),
				(re.compile("[0-9]*\\.[0-9]+([eE][-+][0-9]+)?"), "number", "php"),
				(re.compile("[0-9]*\\.[0-9]+([eE][-+][0-9]+)?[Ff]"), "number", "php"),
				
				# everything else 
				(re.compile(r'\s+'), None, "js"),				# whitespace
				(re.compile(r'\/\/[\w\W]*?(\n|$)'), "comment", "php"),		# single-line comment
				(re.compile(r'\/\*[\w\W]*?(\*\/|$)'), "comment", "php"),	# block comment
				(re.compile(r'[_a-zA-Z][_a-zA-Z0-9]*'), None, "php"),		# identifier
				(re.compile(r'[ru]?"(\\.|.)*?("|$|\n)'), "string", "php"),	# double-quote string
				(re.compile(r'[ru]?\'(\\.|.)*?(\'|$|\n)'), "string", "php"),	# single-quote string
				(re.compile(r'\$[_a-zA-Z][_a-zA-Z0-9]*'), "userType", "php"),	# variable name ($)
			],
		}
		
		self.stateTable[0] = self.stateTable[initState]

	def getContextualNewline(self, before, after):
		lastLine = before.split("\n")[-1]
		tabDepth = len(lastLine) - len(lastLine.lstrip("\t"))
		beforeText = "\t" * tabDepth
		afterText = ""
		
		notabs = lastLine.lstrip("\t")
		
		if before.endswith("{"):
			afterText = "\n" + beforeText
			beforeText += "\t"
			if not after.startswith("}"):
				afterText += "}"
		elif before.endswith("/**"):
			afterText = "\n" + beforeText
			beforeText += " * "
			afterText += " */"
		elif notabs.startswith(" * "):
			beforeText += " * "

		return (beforeText, afterText)

	def isForFile(self, filename, content):
		if filename.endswith(".php") and self.initState == "html":
			return True
		return filename.endswith("." + self.initState)
	
	def getIconName(self, filename, content):
		return self.iconName