"""
	Madd Studio

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

from gettext import *
from mstudio.WorkspacePicker import *
import sys, os
import json
import wx
import random
import string

app = wx.App(False)

DEFAULT_CONFIG = {
	"CodeEditor.TabLen": 8,
	"CodeEditor.FontFamily": "Ubuntu Mono",
	"CodeEditor.FontSize": 13,
	"CodeEditor.Background": (250, 250, 250),
	"CodeEditor.Foreground": (0, 0, 0),
	"CodeEditor.SelectionBackground": (17, 238, 238),
	"CodeEditor.Syntax.Keyword": (51, 51, 255),
	"CodeEditor.Syntax.String": (34, 136, 34),
	"CodeEditor.Syntax.Number": (170, 0, 170),
	"CodeEditor.Syntax.UserType": (85, 136, 136),
	"CodeEditor.Syntax.Comment": (200, 127, 0),
	"CodeEditor.Syntax.Preprocessor": (170, 170, 170),
	"CodeEditor.Syntax.Error": (255, 0, 0),
	"CodeEditor.Syntax.LocalVar": (84, 124, 31),
	"CodeEditor.Syntax.Field": (123, 62, 131),
	"CodeEditor.Syntax.Method": (49, 147, 71),
	
	"CodeEditor.SemanticHighlighting": True,
	"CodeEditor.FileModes": {},
	
	"Debug.Enable": True,
	
	"MainFrame.NavigatorSash": 300,
	"MainFrame.CodeSash": 500,
	
	"Find.AutoWrap": True,
	"Find.CaseSensitive": False,
	
	"Projects": {},
	
	"Build.ConsolePrompt": '\\[\x1b[1m\\]\\[\x1b[34m\\]${PROJECT_NAME}\\[\x1b(B\x1b[m\\]::\\[\x1b[1m\\]\\[\x1b[32m\\]${BUILD_CONFIG}\\[\x1b(B\x1b[m\\]::\\[\x1b[33m\\]\\W\\[\x1b(B\x1b[m\\]\\$ '
}

cmdLineParams = {}
for arg in sys.argv[1:]:
	if arg.startswith("--") and "=" in arg:
		key, value = arg[2:].split("=", 1)
		cmdLineParams[key] = value
	
workspaceSavePath = None
workspacePath = None
defaultWorkspace = None
currentWorkspace = None

if "USERPROFILE" in os.environ:
	workspaceSavePath = os.path.join(os.environ["USERPROFILE"], "MaddStudio.workspace")
	defaultWorkspace = os.environ["USERPROFILE"]
elif "HOME" in os.environ:
	workspaceSavePath = os.path.join(os.environ["HOME"], ".MaddStudio.workspace")
	defaultWorkspace = os.environ["HOME"]

try:
	f = open(workspaceSavePath, "r")
	currentWorkspace = f.readline()
	f.close()
except:
	currentWorkspace = defaultWorkspace

if "workspace" in cmdLineParams:
	currentWorkspace = cmdLineParams["workspace"]

dlg = WorkspacePicker(currentWorkspace)
if dlg.ShowModal() != wx.ID_OK:
	dlg.Destroy()
	sys.exit(0)

currentWorkspace = dlg.getPath()

if dlg.getShouldRemember():
	try:
		f = open(workspaceSavePath, "w")
		f.write(currentWorkspace)
		f.close()
	except:
		pass

dlg.Destroy()

configFilePath = os.path.join(currentWorkspace, "mstudio-workspace.json")
tempDirPath = os.path.join(currentWorkspace, "mstudio-temp")
userPluginsPath = os.path.join(currentWorkspace, "mstudio-plugins")

try:
	os.mkdir(userPluginsPath)
except:
	pass

try:
	os.mkdir(tempDirPath)
except:
	pass

for name in os.listdir(tempDirPath):
	if not name.startswith("."):
		try:
			os.remove(os.path.join(tempDirPath, name))
		except:
			pass

userConfig = None
try:
	f = open(configFilePath, "r")
	userConfig = json.load(f)
	f.close()
except:
	userConfig = {}

for key, value in DEFAULT_CONFIG.items():
	if not (key in userConfig):
		userConfig[key] = value

def SaveWorkspace():
	f = open(configFilePath, "w")
	json.dump(userConfig, f, sort_keys=True, indent=4)
	f.close()

def GetTempFile(suffix):
	name = ''.join(random.choice(string.ascii_lowercase) for i in range(32)) + suffix
	path = os.path.join(tempDirPath, name)
	f = open(path, "wb")
	f.close()
	return path
	
SaveWorkspace()
