"""
	Madd Studio

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

from mstudio.EditorMode import EditorMode, SemanticToken
from mstudio.MBSMetaParser import MBSMetaParser
from mstudio.UserConfig import GetTempFile, tempDirPath
import xml.etree.ElementTree as ET
import sys, os
import re
import wx
import subprocess

"""Editor mode for Perun source code.

"""
class EditorModePerun(EditorMode):
	def __init__(self):
		self.compilerProcess = None
		self.stateTable = [
			[
				# keywords
				(re.compile("namespace"), "keyword", 0),
				(re.compile("import"), "keyword", 0),
				(re.compile("private"), "keyword", 0),
				(re.compile("protected"), "keyword", 0),
				(re.compile("public"), "keyword", 0),
				(re.compile("static"), "keyword", 0),
				(re.compile("abstract"), "keyword", 0),
				(re.compile("final"), "keyword", 0),
				(re.compile("override"), "keyword", 0),
				(re.compile("class"), "keyword", 0),
				(re.compile("extends"), "keyword", 0),
				(re.compile("true"), "userType", 0),
				(re.compile("false"), "userType", 0),
				(re.compile("null"), "userType", 0),
				(re.compile("is"), "keyword", 0),
				(re.compile("new"), "keyword", 0),
				(re.compile("this"), "userType", 0),
				(re.compile("if"), "keyword", 0),
				(re.compile("else"), "keyword", 0),
				(re.compile("while"), "keyword", 0),
				(re.compile("for"), "keyword", 0),
				(re.compile("break"), "keyword", 0),
				(re.compile("continue"), "keyword", 0),
				(re.compile("return"), "keyword", 0),
				(re.compile("try"), "keyword", 0),
				(re.compile("catch"), "keyword", 0),
				(re.compile("throw"), "keyword", 0),
				(re.compile("do"), "keyword", 0),
				(re.compile("destructor"), "keyword", 0),
				(re.compile("extern"), "keyword", 0),
				(re.compile("function"), "keyword", 0),
				(re.compile("lambda"), "keyword", 0),
				(re.compile("foreach"), "keyword", 0),
				(re.compile("typedef"), "keyword", 0),

				# future reserved keywords
				(re.compile("const"), "error", 0),
				(re.compile("export"), "error", 0),
				(re.compile("switch"), "error", 0),
				(re.compile("case"), "error", 0),
				(re.compile("default"), "error", 0),
				(re.compile("enum"), "error", 0),
				(re.compile("struct"), "error", 0),
				(re.compile("auto"), "error", 0),

				# primitive type names
				(re.compile("void"), "keyword", 0),
				(re.compile("bool"), "keyword", 0),
				(re.compile("float"), "keyword", 0),
				(re.compile("double"), "keyword", 0),
				(re.compile("byte_t"), "keyword", 0),
				(re.compile("sbyte_t"), "keyword", 0),
				(re.compile("word_t"), "keyword", 0),
				(re.compile("sword_t"), "keyword", 0),
				(re.compile("dword_t"), "keyword", 0),
				(re.compile("sdword_t"), "keyword", 0),
				(re.compile("qword_t"), "keyword", 0),
				(re.compile("sqword_t"), "keyword", 0),
				(re.compile("int"), "keyword", 0),
				(re.compile("long"), "keyword", 0),
				(re.compile("ptr_t"), "keyword", 0),
				(re.compile("sptr_t"), "keyword", 0),
				
				# numbers
				(re.compile("(0x[0-9a-fA-F]+|0[0-7]*|[1-9][0-9]*)"), "number", 0),
				(re.compile("(0x[0-9a-fA-F]+|0[0-7]*|[1-9][0-9]*)[uU]"), "number", 0),
				(re.compile("(0x[0-9a-fA-F]+|0[0-7]*|[1-9][0-9]*)[lL]"), "number", 0),
				(re.compile("(0x[0-9a-fA-F]+|0[0-7]*|[1-9][0-9]*)[uU][lL]"), "number", 0),
				(re.compile("[0-9]*\\.[0-9]+([eE][-+][0-9]+)?"), "number", 0),
				(re.compile("[0-9]*\\.[0-9]+([eE][-+][0-9]+)?[Ff]"), "number", 0),

				# everything that isn't a keyword
				(re.compile(r'\s+'), None, 0),					# whitespace
				(re.compile(r'\/\/[\w\W]*?(\n|$)'), "comment", 0),		# single-line comment
				(re.compile(r'\/\*[\w\W]*?(\*\/|$)'), "comment", 0),		# block comment
				(re.compile(r'[_a-zA-Z][_a-zA-Z0-9]*'), None, 0),		# identifier
				(re.compile(r'[ru]?"(\\.|.)*?("|$|\n)'), "string", 0),		# double-quote string
				(re.compile(r'\$[_0-9a-zA-Z]*'), "preprocessor", 0),		# build variable
			]
		]

	def getContextualNewline(self, before, after):
		lastLine = before.split("\n")[-1]
		tabDepth = len(lastLine) - len(lastLine.lstrip("\t"))
		beforeText = "\t" * tabDepth
		afterText = ""
		
		notabs = lastLine.lstrip("\t")
		
		if before.endswith("{"):
			afterText = "\n" + beforeText
			beforeText += "\t"
			if not after.startswith("}"):
				afterText += "}"
		elif before.endswith("/**"):
			afterText = "\n" + beforeText
			beforeText += " * "
			afterText += " */"
		elif notabs.startswith(" * "):
			beforeText += " * "

		return (beforeText, afterText)
	
	def isForFile(self, filename, content):
		if filename.endswith(".per"):
			return True
		else:
			return False
	
	def getIconName(self, filename, content):
		return "Perun"

	def beginSemanticHighlight(self, buildDir, fileBaseName, content, callback):
		# Get temporary files
		tagsFilePath = GetTempFile("_tags.xml")
		perFilePath = os.path.join(tempDirPath, fileBaseName)
		
		# Set up the arguments to call the Perun compiler with
		args = ["perun-com", "-fdump-emit", "-fdump-tags=" + tagsFilePath]
		parser = MBSMetaParser()
		parser.on("perun.classpath", lambda classDir: args.append("-C" + classDir))
		parser.on("perun.buildvar", lambda buildVarSetting: args.append("-B" + buildVarSetting))
		parser.feed(os.path.join(buildDir, "mbs.meta"))
		args.append(perFilePath)
		
		# write the Perun code to a file
		f = open(perFilePath, "w")
		f.write(content)
		f.close()
		
		if self.compilerProcess is not None:
			self.compilerProcess.kill()
			self.compilerProcess = None
		
		# create the compiler process
		try:
			proc = subprocess.Popen(args, cwd=buildDir)
		except:
			os.remove(tagsFilePath)
			os.remove(perFilePath)
			return
		
		self.compilerProcess = proc
		
		def pollProcess():
			status = proc.poll()
			if status is None:
				wx.CallLater(100, pollProcess)
			else:
				if self.compilerProcess == proc:
					self.compilerProcess = None
				
				if status == 0:
					f = open(tagsFilePath, "r")
					tagData = f.read()
					f.close()
					
					os.remove(perFilePath)
					os.remove(tagsFilePath)
					
					self.processTagData(tagData, perFilePath, content, callback)

		wx.CallLater(100, pollProcess)
	
	def computeStartPos(self, content, lineno, col):
		lines = content.splitlines()
		dataBefore = 0
		
		for i in range(0, lineno-1):
			dataBefore += len(lines[i]) + 1
		
		return dataBefore + col - 1
	
	def getSemanticAttr(self, tokenTag):
		for node in tokenTag:
			if node.tag == "class_name":
				return "userType"
			elif node.tag == "var_name":
				return "localVar"
			elif node.tag == "field_name":
				return "field"
			elif node.tag == "method_name":
				return "method"
		
		return None
		
	def processTagData(self, tagData, perFilePath, content, callback):
		root = ET.fromstring(tagData)
		semTokens = []
		for tokenTag in root:
			if tokenTag.tag == 'token':
				position = tokenTag.attrib['position']
				filename, linenoSpec, colSpec, lenSpec = position.split(":")
				
				startPos = self.computeStartPos(content, int(linenoSpec), int(colSpec))
				length = int(lenSpec)
				
				attrName = self.getSemanticAttr(tokenTag)
				if attrName is not None:
					semTokens.append(SemanticToken(startPos, length, attrName))
		
		callback(semTokens)