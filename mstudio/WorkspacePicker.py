"""
	Madd Studio

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

import sys, os
import wx

"""A dialog for picking a workspace."""
class WorkspacePicker(wx.Dialog):
	def __init__(self, defaultWorkspace):
		wx.Dialog.__init__(self, None)
		self.SetTitle("Select a workspace")
		
		sizer = wx.BoxSizer(wx.VERTICAL)
		
		lbl = wx.StaticText(self, -1, "Please specify the path to be used as the workspace directory. This directory is used to store all settings.")
		sizer.Add(lbl, 0, wx.ALL | wx.EXPAND, 10)
		
		self.ctrlDir = wx.DirPickerCtrl(self, style=wx.DIRP_DEFAULT_STYLE | wx.DIRP_USE_TEXTCTRL)
		self.ctrlDir.SetPath(defaultWorkspace)
		sizer.Add(self.ctrlDir, 0, wx.ALL | wx.EXPAND, 10)
		
		self.cbRemember = wx.CheckBox(self, label="Set as the default")
		self.cbRemember.SetValue(False)
		sizer.Add(self.cbRemember, 0, wx.LEFT | wx.RIGHT | wx.EXPAND, 10)
		
		btnSizer = self.CreateSeparatedButtonSizer(wx.OK | wx.CANCEL)
		sizer.Add(btnSizer, 0, wx.ALL | wx.EXPAND, 10)
		
		sizer.SetSizeHints(self)
		self.SetSizer(sizer)
		
		self.ctrlDir.SetFocus()
	
	def getPath(self):
		return self.ctrlDir.GetPath()

	def getShouldRemember(self):
		return self.cbRemember.GetValue()