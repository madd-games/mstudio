"""
	Madd Studio

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

from mstudio.UserConfig import *
import wx

"""The 'find' dialog for the editor.

It's not modal!
"""
class FindDialog(wx.Dialog):
	def __init__(self, parent, editor):
		wx.Dialog.__init__(self, parent, -1)
		self.editor = editor
		self.parent = parent
		self.SetTitle("Find...")
		self.ToggleWindowStyle(wx.STAY_ON_TOP)
		self.SetAffirmativeId(wx.ID_FIND)
		
		self.ID_WRAP = self.NewControlId()
		self.ID_CASE_SENSITIVE = self.NewControlId()
		
		masterSizer = wx.BoxSizer(wx.VERTICAL)
		
		self.ctrlNeedle = wx.TextCtrl(self, size=(300, -1), style=wx.TE_PROCESS_ENTER)
		masterSizer.Add(self.ctrlNeedle, 0, wx.ALL|wx.EXPAND, 10)
		self.ctrlNeedle.SetHint("Enter phrase to search...")
		self.ctrlNeedle.Bind(wx.EVT_TEXT_ENTER, self.onFind)
		
		optionSizer = wx.FlexGridSizer(2)
		masterSizer.Add(optionSizer, 1, wx.ALL|wx.EXPAND, 10)
		
		self.cbWrap = wx.CheckBox(self, self.ID_WRAP, label = "Wrap search")
		optionSizer.Add(self.cbWrap, 0, 0, 0)
		self.cbWrap.SetValue(userConfig["Find.AutoWrap"])
		
		self.cbCaseSensitive = wx.CheckBox(self, self.ID_CASE_SENSITIVE, label = "Case sensitive search")
		optionSizer.Add(self.cbCaseSensitive, 0, 0, 0)
		self.cbCaseSensitive.SetValue(userConfig["Find.CaseSensitive"])
		
		line = wx.StaticLine(self)
		masterSizer.Add(line, 0, wx.ALL|wx.EXPAND, 10)
		
		btnSizer = wx.BoxSizer(wx.HORIZONTAL)
		masterSizer.Add(btnSizer, 0, wx.ALL|wx.ALIGN_RIGHT, 10)
		
		self.btnClose = wx.Button(self, wx.ID_CLOSE)
		btnSizer.Add(self.btnClose, 0, wx.ALL, 2)

		self.btnFind = wx.Button(self, wx.ID_FIND)
		btnSizer.Add(self.btnFind, 0, wx.ALL, 2)

		masterSizer.SetSizeHints(self)
		self.SetSizer(masterSizer)

		self.Bind(wx.EVT_BUTTON, self.onClose, id = wx.ID_CLOSE)
		self.Bind(wx.EVT_BUTTON, self.onFind, id = wx.ID_FIND)
		
		self.Bind(wx.EVT_CHECKBOX, self.onToggleWrap, id = self.ID_WRAP)
		self.Bind(wx.EVT_CHECKBOX, self.onToggleCaseSensitive, id = self.ID_CASE_SENSITIVE)
	
	def doSearch(self, content, needle, begin):
		if not self.cbCaseSensitive.IsChecked():
			content = content.lower()
			needle = needle.lower()

		pos = content.find(needle, begin)
		if pos == -1:
			return (-1, -1)
		else:
			return (pos, pos+len(needle))
		
	def onFind(self, e):
		if self.editor.file is None:
			return
		
		content = self.editor.GetValue()
		begin = self.editor.GetSelection()[1]
		
		needle = self.ctrlNeedle.GetValue()
		pos, end = self.doSearch(content, needle, begin)
		
		if pos == -1:
			if self.cbWrap.IsChecked():
				pos, end = self.doSearch(content, needle, 0)
		
		if pos == -1:
			wx.MessageBox("No results found", "Madd Studio", wx.ICON_ERROR)
		else:
			self.editor.GotoPos(end)
			self.editor.SetSelection(pos, end)
		
	def onClose(self, e):
		self.parent.findDialog = None
		self.Destroy()

	def onToggleWrap(self, e):
		userConfig["Find.AutoWrap"] = self.cbWrap.IsChecked()
		SaveWorkspace()
		
	def onToggleCaseSensitive(self, e):
		userConfig["Find.CaseSensitive"] = self.cbCaseSensitive.IsChecked()
		SaveWorkspace()
		
