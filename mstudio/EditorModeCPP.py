"""
	Madd Studio

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

from mstudio.EditorMode import *
import re

"""Editor mode for C/C++ files.

"""
class EditorModeCPP(EditorMode):
	def __init__(self, enableCPP):
		EditorMode.__init__(self)
		self.enableCPP = enableCPP
		
		self.stateTable = [
			[
				# match keywords first, so that the identifiers later don't
				# override them
				(re.compile(r'asm'), "keyword", 0),
				(re.compile(r'auto'), "keyword", 0),
				(re.compile(r'break'), "keyword", 0),
				(re.compile(r'case'), "keyword", 0),
				(re.compile(r'char'), "keyword", 0),
				(re.compile(r'const'), "keyword", 0),
				(re.compile(r'continue'), "keyword", 0),
				(re.compile(r'default'), "keyword", 0),
				(re.compile(r'do'), "keyword", 0),
				(re.compile(r'double'), "keyword", 0),
				(re.compile(r'else'), "keyword", 0),
				(re.compile(r'enum'), "keyword", 0),
				(re.compile(r'extern'), "keyword", 0),
				(re.compile(r'float'), "keyword", 0),
				(re.compile(r'for'), "keyword", 0),
				(re.compile(r'goto'), "keyword", 0),
				(re.compile(r'if'), "keyword", 0),
				(re.compile(r'int'), "keyword", 0),
				(re.compile(r'inline'), "keyword", 0),
				(re.compile(r'long'), "keyword", 0),
				(re.compile(r'register'), "keyword", 0),
				(re.compile(r'return'), "keyword", 0),
				(re.compile(r'short'), "keyword", 0),
				(re.compile(r'signed'), "keyword", 0),
				(re.compile(r'sizeof'), "keyword", 0),
				(re.compile(r'static'), "keyword", 0),
				(re.compile(r'struct'), "keyword", 0),
				(re.compile(r'switch'), "keyword", 0),
				(re.compile(r'typedef'), "keyword", 0),
				(re.compile(r'union'), "keyword", 0),
				(re.compile(r'unsigned'), "keyword", 0),
				(re.compile(r'void'), "keyword", 0),
				(re.compile(r'volatile'), "keyword", 0),
				(re.compile(r'while'), "keyword", 0),
				(re.compile(r'NULL'), "userType", 0),
				
				# match everything else
				(re.compile(r'\s+'), None, 0),					# whitespace
				(re.compile(r'\/\/[\w\W]*?(\n|$)'), "comment", 0),		# single-line comment
				(re.compile(r'\/\*[\w\W]*?(\*\/|$)'), "comment", 0),		# block comment
				(re.compile(r'\#[\w\W]*?(\n|$)'), "preprocessor", 0),		# preprocessor
				(re.compile(r'[_a-zA-Z][_a-zA-Z0-9]*'), None, 0),		# identifier
				(re.compile(r'"(\\.|.)*?("|$|\n)'), "string", 0),		# double-quote string
				(re.compile(r'\'(\\.|.)(\'|$|\n)'), "string", 0),		# char
			]
		]
		
		if enableCPP:
			self.stateTable[0][:0] = [
				# C++-only keywords
				(re.compile(r'new'), "keyword", 0),
				(re.compile(r'this'), "keyword", 0),
				(re.compile(r'operator'), "keyword", 0),
				(re.compile(r'throw'), "keyword", 0),
				(re.compile(r'bool'), "keyword", 0),
				(re.compile(r'explicit'), "keyword", 0),
				(re.compile(r'private'), "keyword", 0),
				(re.compile(r'true'), "userType", 0),
				(re.compile(r'export'), "keyword", 0),
				(re.compile(r'protected'), "keyword", 0),
				(re.compile(r'try'), "keyword", 0),
				(re.compile(r'public'), "keyword", 0),
				(re.compile(r'catch'), "keyword", 0),
				(re.compile(r'false'), "userType", 0),
				(re.compile(r'typeid'), "keyword", 0),
				(re.compile(r'reinterpret_cast'), "keyword", 0),
				(re.compile(r'typename'), "keyword", 0),
				(re.compile(r'class'), "keyword", 0),
				(re.compile(r'friend'), "keyword", 0),
				(re.compile(r'const_cast'), "keyword", 0),
				(re.compile(r'using'), "keyword", 0),
				(re.compile(r'virtual'), "keyword", 0),
				(re.compile(r'delete'), "keyword", 0),
				(re.compile(r'static_cast'), "keyword", 0),
				(re.compile(r'wchar_t'), "keyword", 0),
				(re.compile(r'mutable'), "keyword", 0),
				(re.compile(r'dynamic_cast'), "keyword", 0),
				(re.compile(r'namespace'), "keyword", 0),
				(re.compile(r'template'), "keyword", 0)
			]
		
	def getContextualNewline(self, before, after):
		lastLine = before.split("\n")[-1]
		tabDepth = len(lastLine) - len(lastLine.lstrip("\t"))
		beforeText = "\t" * tabDepth
		afterText = ""
		
		notabs = lastLine.lstrip("\t")
		
		if before.endswith("{"):
			afterText = "\n" + beforeText
			beforeText += "\t"
			if not after.startswith("}"):
				afterText += "}"
		elif before.endswith("/**"):
			afterText = "\n" + beforeText
			beforeText += " * "
			afterText += " */"
		elif notabs.startswith(" * "):
			beforeText += " * "

		return (beforeText, afterText)
	
	def isForFile(self, filename, content):
		if filename.endswith(".c"):
			return not self.enableCPP
		elif filename.endswith(".h") or filename.endswith(".cpp") or filename.endswith(".hpp"):
			return self.enableCPP
		else:
			return False
	
	def getIconName(self, filename, content):
		if filename.endswith(".c"):
			return "C"
		elif filename.endswith(".h") or filename.endswith(".hpp"):
			return "C/C++ Header"
		else:
			return "C++"
