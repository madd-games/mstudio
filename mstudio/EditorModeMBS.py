"""
	Madd Studio

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

from mstudio.EditorMode import *
import re

"""Editor mode for MBS files.

"""
class EditorModeMBS(EditorMode):
	def __init__(self):
		EditorMode.__init__(self)
		self.stateTable = [
			[
				# match keywords first, so that the identifiers later don't
				# override them
				(re.compile(r'set'), "keyword", 0),
				(re.compile(r'package'), "keyword", 0),
				(re.compile(r'file'), "keyword", 0),
				(re.compile(r'makerule'), "keyword", 0),
				(re.compile(r'print'), "keyword", 0),
				(re.compile(r'import'), "keyword", 0),
				(re.compile(r'export'), "keyword", 0),
				(re.compile(r'macro'), "keyword", 0),
				(re.compile(r'switch'), "keyword", 0),
				(re.compile(r'case'), "keyword", 0),
				(re.compile(r'break'), "keyword", 0),
				(re.compile(r'error'), "keyword", 0),
				(re.compile(r'unit_test'), "keyword", 0),
				(re.compile(r'meta'), "keyword", 0),
				
				(re.compile(r'mbs_release_version'), "userType", 0),
				
				# match everything else
				(re.compile(r'\s+'), None, 0),					# whitespace
				(re.compile(r'\/\/[\w\W]*?(\n|$)'), "comment", 0),		# single-line comment
				(re.compile(r'\/\*[\w\W]*?(\*\/|$)'), "comment", 0),		# block comment
				(re.compile(r'\#[\w\W]*?(\n|$)'), "comment", 0),		# single-line comment with '#'
				(re.compile(r'[_a-zA-Z][_a-zA-Z0-9]*'), None, 0),		# identifier
				(re.compile(r'"(\\.|.)*?("|$|\n)'), "string", 0),		# double-quote string
				(re.compile(r'\'(\\.|.)*?(\'|$\n)'), "string", 0),		# string
			]
		]
		
		self.prefix = re.compile(r'\#\!\s*\/usr\/bin\/mbs')

	def getContextualNewline(self, before, after):
		lastLine = before.split("\n")[-1]
		tabDepth = len(lastLine) - len(lastLine.lstrip("\t"))
		beforeText = "\t" * tabDepth
		afterText = ""
		
		if before.endswith("{"):
			afterText = "\n" + beforeText
			beforeText += "\t"
			if not after.startswith("}"):
				afterText += "}"
			
		return (beforeText, afterText)
	
	def isForFile(self, filename, content):
		if filename.endswith(".mbs"):
			return True
		elif self.prefix.match(content) is not None:
			return True
		else:
			return False
	
	def getIconName(self, filename, content):
		return "MBS"
