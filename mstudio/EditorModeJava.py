"""
	Madd Studio

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

from mstudio.EditorMode import *
import re

"""Editor mode for Java files.

"""
class EditorModeJava(EditorMode):
	def __init__(self):
		EditorMode.__init__(self)
		
		self.stateTable = [
			[
				# match keywords first, so that the identifiers later don't
				# override them
				(re.compile(r'abstract'), "keyword", 0),
				(re.compile(r'assert'), "keyword", 0),
				(re.compile(r'boolean'), "keyword", 0),
				(re.compile(r'break'), "keyword", 0),
				(re.compile(r'byte'), "keyword", 0),
				(re.compile(r'case'), "keyword", 0),
				(re.compile(r'catch'), "keyword", 0),
				(re.compile(r'char'), "keyword", 0),
				(re.compile(r'class'), "keyword", 0),
				(re.compile(r'continue'), "keyword", 0),
				(re.compile(r'default'), "keyword", 0),
				(re.compile(r'do'), "keyword", 0),
				(re.compile(r'double'), "keyword", 0),
				(re.compile(r'else'), "keyword", 0),
				(re.compile(r'enum'), "keyword", 0),
				(re.compile(r'extends'), "keyword", 0),
				(re.compile(r'final'), "keyword", 0),
				(re.compile(r'finally'), "keyword", 0),
				(re.compile(r'float'), "keyword", 0),
				(re.compile(r'for'), "keyword", 0),
				(re.compile(r'if'), "keyword", 0),
				(re.compile(r'implements'), "keyword", 0),
				(re.compile(r'import'), "keyword", 0),
				(re.compile(r'instanceof'), "keyword", 0),
				(re.compile(r'int'), "keyword", 0),
				(re.compile(r'interface'), "keyword", 0),
				(re.compile(r'long'), "keyword", 0),
				(re.compile(r'native'), "keyword", 0),
				(re.compile(r'new'), "keyword", 0),
				(re.compile(r'null'), "userType", 0),
				(re.compile(r'true'), "userType", 0),
				(re.compile(r'false'), "userType", 0),
				(re.compile(r'package'), "keyword", 0),
				(re.compile(r'private'), "keyword", 0),
				(re.compile(r'protected'), "keyword", 0),
				(re.compile(r'public'), "keyword", 0),
				(re.compile(r'return'), "keyword", 0),
				(re.compile(r'short'), "keyword", 0),
				(re.compile(r'static'), "keyword", 0),
				(re.compile(r'strictfp'), "keyword", 0),
				(re.compile(r'super'), "userType", 0),
				(re.compile(r'switch'), "keyword", 0),
				(re.compile(r'synchronized'), "keyword", 0),
				(re.compile(r'this'), "userType", 0),
				(re.compile(r'throw'), "keyword", 0),
				(re.compile(r'throws'), "keyword", 0),
				(re.compile(r'transient'), "keyword", 0),
				(re.compile(r'try'), "keyword", 0),
				(re.compile(r'void'), "keyword", 0),
				(re.compile(r'volatile'), "keyword", 0),
				(re.compile(r'while'), "keyword", 0),
				(re.compile(r'const'), "keyword", 0),
				(re.compile(r'goto'), "keyword", 0),

				# match everything else
				(re.compile(r'\s+'), None, 0),					# whitespace
				(re.compile(r'\/\/[\w\W]*?(\n|$)'), "comment", 0),		# single-line comment
				(re.compile(r'\/\*[\w\W]*?(\*\/|$)'), "comment", 0),		# block comment
				(re.compile(r'[_a-zA-Z][_a-zA-Z0-9]*'), None, 0),		# identifier
				(re.compile(r'"(\\.|.)*?("|$|\n)'), "string", 0),		# double-quote string
				(re.compile(r'\'(\\.|.)(\'|$|\n)'), "string", 0),		# char
			]
		]
		
	def getContextualNewline(self, before, after):
		lastLine = before.split("\n")[-1]
		tabDepth = len(lastLine) - len(lastLine.lstrip("\t"))
		beforeText = "\t" * tabDepth
		afterText = ""
		
		if before.endswith("{"):
			afterText = "\n" + beforeText
			beforeText += "\t"
			if not after.startswith("}"):
				afterText += "}"
			
		return (beforeText, afterText)
	
	def isForFile(self, filename, content):
		if filename.endswith(".java"):
			return True
		else:
			return False
