"""
	Madd Studio

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

from mstudio.Project import Project
from mstudio.UserConfig import *
from mstudio.EditorFile import EditorFile, GLOBAL_FILE_LIST
from mstudio.EditorFolder import EditorFolder
from mstudio.IconList import iconList
from subprocess import Popen
import wx

"""Navigator control, appearing on the left of the editor window.

"""
class Navigator(wx.TreeCtrl):
	def __init__(self, parent, mainFrame):
		wx.TreeCtrl.__init__(self, parent)
		self.editor = None
		self.root = self.AddRoot("Workspace")
		self.projects = []
		self.mainFrame = mainFrame
	
		imageList = wx.ImageList(24, 24)
		self.icons = {}
		
		for iconName, path in iconList.items():
			try:
				self.icons[iconName] = imageList.Add(wx.Image(path, wx.BITMAP_TYPE_PNG).ConvertToBitmap())
			except Exception as e:
				print("Cannot load image %s (%s): %s" % (iconName, path, str(e)))
	
		self.AssignImageList(imageList)
		self.SetItemImage(self.root, self.icons["Madd Studio Logo"], wx.TreeItemIcon_Normal)
		
		for name, info in userConfig["Projects"].items():
			try:
				self.projects.append(Project(self, name, info))
			except Exception as e:
				wx.MessageBox("Failed to load project %s: %s" % (name, str(e)), "Madd Studio", wx.ICON_ERROR)

		self.Bind(wx.EVT_TREE_ITEM_ACTIVATED, self.onActivate)
		self.Bind(wx.EVT_TREE_ITEM_MENU, self.onContextMenu)
		wx.CallLater(1000, self.onTick)
		
	def onTick(self):
		wx.CallLater(1000, self.onTick)
		for p in self.projects:
			p.onTick()
		
	def getSelectedProject(self):
		data = None
		try:
			data = self.GetItemData(self.GetSelection())
		except:
			pass
		
		if data is not None and (isinstance(data, EditorFolder) or isinstance(data, EditorFile)):
			return data.project
		
		return None

	def setEditor(self, editor):
		self.editor = editor
		
	def onActivate(self, e):
		item = e.GetItem()
		data = self.GetItemData(item)
		if isinstance(data, EditorFile):
			self.editor.setFile(data)
			self.editor.SetFocus()
	
	def onContextMenu(self, e):
		item = e.GetItem()
		data = self.GetItemData(item)
		
		menu = wx.Menu()
		if isinstance(data, EditorFolder):
			menu.Append(self.mainFrame.ID_NEW_FOLDER, "New folder...", "Create a new folder inside the selected folder.")
			menu.Append(self.mainFrame.ID_NEW_FILE, "New file...", "Create a new file inside the selected folder.")
			menu.AppendSeparator()
		menu.Append(self.mainFrame.ID_DELETE_FILE, "Delete...", "Delete the selected file or folder.")
		self.PopupMenu(menu)
		
	def goToLocation(self, filename, lineno, offset):
		if filename in GLOBAL_FILE_LIST:
			fdata = GLOBAL_FILE_LIST[filename]
			self.SelectItem(fdata.item)
			self.editor.setFile(fdata)
			self.editor.SetFocus()
			
			lines = self.editor.GetValue().splitlines()
			lineOffset = 0
			while lineno > 1 and len(lines) != 0:
				lineOffset += len(lines[0]) + 1
				lineno -= 1
				lines = lines[1:]
			self.editor.GotoPos(lineOffset + offset - 1)
		
	def hasDirty(self):
		for p in self.projects:
			if p.hasDirty():
				return True
		return False

	def saveAll(self):
		for p in self.projects:
			if not p.saveAll():
				return False
		
		return True

	def newFolder(self):
		data = None
		try:
			data = self.GetItemData(self.GetSelection())
		except:
			pass
		
		if data is not None and isinstance(data, EditorFolder):
			data.newFolder()

	def newFile(self):
		data = None
		try:
			data = self.GetItemData(self.GetSelection())
		except:
			pass
		
		if data is not None and isinstance(data, EditorFolder):
			data.newFile()
	
	def delete(self):
		data = None
		try:
			data = self.GetItemData(self.GetSelection())
		except:
			pass
		
		if data is not None and isinstance(data, EditorFile):
			data.delete()

	def removeProject(self):
		data = None
		try:
			data = self.GetItemData(self.GetSelection())
		except:
			pass
		
		if data is not None and (isinstance(data, EditorFolder) or isinstance(data, EditorFile)):
			proj = data.project
			if wx.MessageBox("You are about to remove project '%s' from the current workspace. This will NOT delete any files, but only unlist the project from the workspace. Do you want to do this?" % proj.name, "Madd Studio", wx.YES_NO | wx.ICON_INFORMATION) == wx.YES:
				del userConfig["Projects"][proj.name]
				SaveWorkspace()
				self.Delete(proj.rootItem)
