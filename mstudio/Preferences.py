"""
	Madd Studio

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

from mstudio.UserConfig import *
from mstudio.Plugin import *
import wx

"""Preferences dialog.

"""
class Preferences(wx.Dialog):
	def __init__(self, parent):
		wx.Dialog.__init__(self, parent, -1)
		self.SetTitle("Preferences")
		self.editorColorList = []
		self.parent = parent
		
		self.COL_PLUGIN_NAME, self.COL_PLUGIN_SOURCE = range(2)
		
		masterSizer = wx.BoxSizer(wx.VERTICAL)
		
		self.nb = wx.Notebook(self)
		masterSizer.Add(self.nb, 1, wx.ALL|wx.EXPAND, 2)
		masterSizer.Add(self.CreateSeparatedButtonSizer(wx.CLOSE), 0, wx.ALL|wx.EXPAND, 2)
		
		editorTab = self.makeEditorTab(self.nb)
		self.nb.AddPage(editorTab, "Editor")
		
		pluginsTab = self.makePluginsTab(self.nb)
		self.nb.AddPage(pluginsTab, "Plugins")
		
		masterSizer.SetSizeHints(self)
		self.SetSizer(masterSizer)
		
		self.Bind(wx.EVT_COLOURPICKER_CHANGED, self.onUpdate)
	
	def onUpdate(self, e):
		for prop, cp in self.editorColorList:
			color = cp.GetColour()
			userConfig[prop] = (color.Red(), color.Green(), color.Blue())
		
		SaveWorkspace()
		self.parent.editor.reloadConfig()
	
	def onResetProp(self, e):
		selectedProp = e.GetEventObject().prop
		for prop, cp in self.editorColorList:
			if prop == selectedProp:
				cp.SetColour(wx.Colour(*DEFAULT_CONFIG[prop]))
				userConfig[prop] = DEFAULT_CONFIG[prop]
				SaveWorkspace()
				self.parent.editor.reloadConfig()
				break
		
	def makeEditorTab(self, parent):
		tab = wx.Panel(parent)
		
		table = wx.FlexGridSizer(3)
		tab.SetSizer(table)
		
		colorList = [
			('Editor background', "CodeEditor.Background"),
			('Normal text color', "CodeEditor.Foreground"),
			('Keywords', "CodeEditor.Syntax.Keyword"),
			('Strings', "CodeEditor.Syntax.String"),
			('Numbers', "CodeEditor.Syntax.Number"),
			('Library/user types/constants', "CodeEditor.Syntax.UserType"),
			('Comments', "CodeEditor.Syntax.Comment"),
			('Preprocessor directive', "CodeEditor.Syntax.Preprocessor"),
			('Invalid syntax', "CodeEditor.Syntax.Error"),
			('Local variable', "CodeEditor.Syntax.LocalVar"),
			('Field name', 'CodeEditor.Syntax.Field'),
			('Method name', 'CodeEditor.Syntax.Method'),
			('Selection background', "CodeEditor.SelectionBackground"),
		]
		
		self.ID_COLOR_RESET = self.NewControlId()
		self.Bind(wx.EVT_BUTTON, self.onResetProp, id = self.ID_COLOR_RESET)
		
		for lbl, prop in colorList:
			txt = wx.StaticText(tab, -1, lbl + ": ")
			table.Add(txt, 0, wx.ALL|wx.ALIGN_LEFT, 10)
			
			cp = wx.ColourPickerCtrl(tab)
			table.Add(cp, 1, wx.ALL, 10)
			cp.SetColour(wx.Colour(*userConfig[prop]))
			
			rbtn = wx.Button(tab, self.ID_COLOR_RESET, "Reset to default")
			table.Add(rbtn, 0, wx.ALL, 10)
			rbtn.prop = prop
			
			self.editorColorList.append((prop, cp))
		
		return tab
	
	def makePluginsTab(self, parent):
		s = wx.BoxSizer(wx.VERTICAL)
		parent.SetSizer(s)
		
		listCtrl = wx.ListCtrl(parent, style=wx.LC_REPORT)
		s.Add(listCtrl, 1, wx.ALL | wx.EXPAND, 0)
		
		listCtrl.InsertColumn(self.COL_PLUGIN_NAME, "Name", width=100)
		listCtrl.InsertColumn(self.COL_PLUGIN_SOURCE, "Source", width=200)
		
		index = 0
		for plugin in loadedPlugins:
			listCtrl.InsertItem(index, plugin.name)
			listCtrl.SetItem(index, self.COL_PLUGIN_SOURCE, plugin.path)
			index += 1
		
		return listCtrl
