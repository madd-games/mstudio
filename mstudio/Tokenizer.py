"""
	Madd Studio

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

"""A class which helps with tokenization for editor modes.

A tokenizer is always in a "state". At the beginning of a file, it is in state 0. All
other states have meaning decided by the editor mode itself. Editor modes can analyse
texts at the current "cursor position", and move it forward via skip(), or they can call
emit() instead to colorize the text!
"""
class Tokenizer:
	def __init__(self, code):
		self.code = code
		self.state = 0
		self.cursor = 0
		self.endPos = -1
		self.tokens = []
	
	"""Check if the specified regular expression matches the next token.
	
	Returns 0 if the text at the current position does not match the specified
	pattern. Otherwise, returns the number of characters that do.
	"""
	def hasMatch(self, pattern):
		m = pattern.match(self.code, self.cursor)
		if m is None:
			return 0
		
		return m.end() - m.start()
	
	"""Move the cursor forward."""
	def skip(self, count):
		self.cursor += count
	
	"""Emit a token and move the cursor forward.
	
	'count' is the number of characters constituting the next token at the cursor
	position. 'attr' is a string describing which type the token is.
	"""
	def emit(self, count, attr, stateAfter):
		self.tokens.append((self.cursor, self.cursor+count, attr, stateAfter))
		self.state = stateAfter
		self.cursor += count
	
	"""Return true if we are at the end."""
	def end(self):
		return self.cursor == len(self.code) or (self.endPos != -1 and self.cursor >= self.endPos)
