"""
	Madd Studio

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

from mstudio.UserConfig import *
import wx
import wx.stc

"""The 'run configurations' dialog.

Guess what it does."""
class RunConfigs(wx.Dialog):
	def __init__(self, parent, project):
		wx.Dialog.__init__(self, parent)
		self.SetTitle("%s - Run configurations" % project.name)
		self.SetMinSize(wx.Size(-1, 400))
		
		topSizer = wx.BoxSizer(wx.VERTICAL)
		
		horSizer = wx.BoxSizer(wx.HORIZONTAL)
		topSizer.Add(horSizer, 1, wx.TOP | wx.LEFT | wx.RIGHT | wx.EXPAND, 10)
		
		leftSizer = wx.BoxSizer(wx.VERTICAL)
		horSizer.Add(leftSizer, 1, wx.RIGHT | wx.EXPAND, 10)
		
		self.lstConfigs = wx.ListBox(self, style=wx.LB_SORT)
		leftSizer.Add(self.lstConfigs, 1, wx.EXPAND, 0)
		
		configButtonSizer = wx.BoxSizer(wx.HORIZONTAL)
		leftSizer.Add(configButtonSizer, 0, wx.TOP | wx.EXPAND, 10)
		
		self.btnAdd = wx.Button(self, wx.ID_ADD)
		configButtonSizer.Add(self.btnAdd, 0, wx.RIGHT, 2)
		
		self.btnRemove = wx.Button(self, wx.ID_REMOVE)
		configButtonSizer.Add(self.btnRemove, 0, 0, 0)
		self.btnRemove.Disable()
		
		rightSizer = wx.BoxSizer(wx.VERTICAL)
		horSizer.Add(rightSizer, 3, wx.EXPAND, 0)
		
		rightSizer.Add(wx.StaticText(self, -1, "Configuration name:"), 0, wx.ALL|wx.EXPAND, 2)
		self.ctrlConfigName = wx.TextCtrl(self)
		self.ctrlConfigName.Disable()
		rightSizer.Add(self.ctrlConfigName, 0, wx.ALL|wx.EXPAND, 2)
		
		rightSizer.Add(wx.StaticText(self, -1, "Build:"), 0, wx.ALL|wx.EXPAND, 2)
		self.chBuild = wx.Choice(self)
		self.chBuild.Disable()
		rightSizer.Add(self.chBuild, 0, wx.ALL|wx.EXPAND, 2)
		
		rightSizer.Add(wx.StaticText(self, -1, "Script:"), 0, wx.ALL|wx.EXPAND, 2)
		self.txtScript = wx.stc.StyledTextCtrl(self)
		self.txtScript.Disable()
		rightSizer.Add(self.txtScript, 1, wx.ALL|wx.EXPAND, 2)
		
		topSizer.Add(self.CreateSeparatedButtonSizer(wx.CLOSE), 0, wx.ALL|wx.EXPAND, 10)
		
		self.SetSizer(topSizer)
		topSizer.SetSizeHints(self)