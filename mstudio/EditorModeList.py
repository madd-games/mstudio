"""
	Madd Studio

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

from mstudio.EditorMode import EditorMode
from mstudio.EditorModeMBS import EditorModeMBS
from mstudio.EditorModePython import EditorModePython
from mstudio.EditorModeCPP import EditorModeCPP
from mstudio.EditorModePerun import EditorModePerun
from mstudio.EditorModeJava import EditorModeJava
from mstudio.EditorModeWeb import EditorModeWeb
from mstudio.EditorModeSQL import EditorModeSQL
from mstudio.EditorModeGLSL import EditorModeGLSL

editorModeList = {
	"None":			EditorMode(),
	"MBS":			EditorModeMBS(),
	"Python":		EditorModePython(),
	"C":			EditorModeCPP(False),
	"C++":			EditorModeCPP(True),
	"Perun":		EditorModePerun(),
	"Java":			EditorModeJava(),
	"HTML":			EditorModeWeb("html", "HTML"),
	"JavaScript":		EditorModeWeb("js", "JavaScript"),
	"PHP":			EditorModeWeb("html", "HTML"),
	"SQL":			EditorModeSQL(),
	"GLSL":			EditorModeGLSL()
}
