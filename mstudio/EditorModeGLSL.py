"""
	Madd Studio

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

from mstudio.EditorMode import *
import re

"""Editor mode for GLSL files.

"""
class EditorModeGLSL(EditorMode):
	def __init__(self):
		EditorMode.__init__(self)
		
		self.stateTable = [
			[
				# match keywords first, so that the identifiers later don't
				# override them
				(re.compile(r'attribute'), "keyword", 0),
				(re.compile(r'const'), "keyword", 0),
				(re.compile(r'uniform'), "keyword", 0),
				(re.compile(r'varying'), "keyword", 0),
				(re.compile(r'layout'), "keyword", 0),
				(re.compile(r'centroid'), "keyword", 0),
				(re.compile(r'flat'), "keyword", 0),
				(re.compile(r'smooth'), "keyword", 0),
				(re.compile(r'noperspective'), "keyword", 0),
				(re.compile(r'break'), "keyword", 0),
				(re.compile(r'continue'), "keyword", 0),
				(re.compile(r'do'), "keyword", 0),
				(re.compile(r'for'), "keyword", 0),
				(re.compile(r'while'), "keyword", 0),
				(re.compile(r'switch'), "keyword", 0),
				(re.compile(r'case'), "keyword", 0),
				(re.compile(r'default'), "keyword", 0),
				(re.compile(r'if'), "keyword", 0),
				(re.compile(r'else'), "keyword", 0),
				(re.compile(r'in'), "keyword", 0),
				(re.compile(r'out'), "keyword", 0),
				(re.compile(r'inout'), "keyword", 0),
				(re.compile(r'float'), "keyword", 0),
				(re.compile(r'int'), "keyword", 0),
				(re.compile(r'void'), "keyword", 0),
				(re.compile(r'bool'), "keyword", 0),
				(re.compile(r'invariant'), "keyword", 0),
				(re.compile(r'discard'), "keyword", 0),
				(re.compile(r'return'), "keyword", 0),
				(re.compile(r'mat2'), "keyword", 0),
				(re.compile(r'mat3'), "keyword", 0),
				(re.compile(r'mat4'), "keyword", 0),
				(re.compile(r'mat2x2'), "keyword", 0),
				(re.compile(r'mat2x3'), "keyword", 0),
				(re.compile(r'mat2x4'), "keyword", 0),
				(re.compile(r'mat3x2'), "keyword", 0),
				(re.compile(r'mat3x3'), "keyword", 0),
				(re.compile(r'mat3x4'), "keyword", 0),
				(re.compile(r'mat4x2'), "keyword", 0),
				(re.compile(r'mat4x3'), "keyword", 0),
				(re.compile(r'mat4x4'), "keyword", 0),
				(re.compile(r'vec2'), "keyword", 0),
				(re.compile(r'vec3'), "keyword", 0),
				(re.compile(r'vec4'), "keyword", 0),
				(re.compile(r'ivec2'), "keyword", 0),
				(re.compile(r'ivec3'), "keyword", 0),
				(re.compile(r'ivec4'), "keyword", 0),
				(re.compile(r'bvec2'), "keyword", 0),
				(re.compile(r'bvec3'), "keyword", 0),
				(re.compile(r'bvec4'), "keyword", 0),
				(re.compile(r'uint'), "keyword", 0),
				(re.compile(r'uvec2'), "keyword", 0),
				(re.compile(r'uvec3'), "keyword", 0),
				(re.compile(r'uvec4'), "keyword", 0),
				(re.compile(r'lowp'), "keyword", 0),
				(re.compile(r'mediump'), "keyword", 0),
				(re.compile(r'highp'), "keyword", 0),
				(re.compile(r'precision'), "keyword", 0),
				(re.compile(r'sampler1D'), "keyword", 0),
				(re.compile(r'sampler2D'), "keyword", 0),
				(re.compile(r'sampler3D'), "keyword", 0),
				(re.compile(r'samplerCube'), "keyword", 0),
				(re.compile(r'sampler1DShadow'), "keyword", 0),
				(re.compile(r'sampler2DShadow'), "keyword", 0),
				(re.compile(r'samplerCubeShadow'), "keyword", 0),
				(re.compile(r'sampler1DArray'), "keyword", 0),
				(re.compile(r'sampler2DArray'), "keyword", 0),
				(re.compile(r'sampler1DArrayShadow'), "keyword", 0),
				(re.compile(r'sampler2DArrayShadow'), "keyword", 0),
				(re.compile(r'isampler1D'), "keyword", 0),
				(re.compile(r'isampler2D'), "keyword", 0),
				(re.compile(r'isampler3D'), "keyword", 0),
				(re.compile(r'isamplerCube'), "keyword", 0),
				(re.compile(r'isampler1DArray'), "keyword", 0),
				(re.compile(r'isampler2DArray'), "keyword", 0),
				(re.compile(r'usampler1D'), "keyword", 0),
				(re.compile(r'usampler2D'), "keyword", 0),
				(re.compile(r'usampler3D'), "keyword", 0),
				(re.compile(r'usamplerCube'), "keyword", 0),
				(re.compile(r'usampler1DArray'), "keyword", 0),
				(re.compile(r'usampler2DArray'), "keyword", 0),
				(re.compile(r'sampler2DRect'), "keyword", 0),
				(re.compile(r'sampler2DRectShadow'), "keyword", 0),
				(re.compile(r'isampler2DRect'), "keyword", 0),
				(re.compile(r'usampler2DRect'), "keyword", 0),
				(re.compile(r'samplerBuffer'), "keyword", 0),
				(re.compile(r'isamplerBuffer'), "keyword", 0),
				(re.compile(r'usamplerBuffer'), "keyword", 0),
				(re.compile(r'struct'), "keyword", 0),

				# boolean constants
				(re.compile(r'true'), "userType", 0),
				(re.compile(r'false'), "userType", 0),
				
				# vertex shader inputs
				(re.compile(r'gl_VertexID'), "userType", 0),
				(re.compile(r'gl_InstanceID'), "userType", 0),
				(re.compile(r'gl_DrawID'), "userType", 0),
				(re.compile(r'gl_BaseVertex'), "userType", 0),
				(re.compile(r'gl_BaseInstance'), "userType", 0),
				
				# vertex ahder outputs
				(re.compile(r'gl_PerVertex'), "userType", 0),
				(re.compile(r'gl_Position'), "userType", 0),
				(re.compile(r'gl_PointSize'), "userType", 0),
				(re.compile(r'gl_ClipDistance'), "userType", 0),
				
				# match everything else
				(re.compile(r'\s+'), None, 0),					# whitespace
				(re.compile(r'\/\/[\w\W]*?(\n|$)'), "comment", 0),		# single-line comment
				(re.compile(r'\/\*[\w\W]*?(\*\/|$)'), "comment", 0),		# block comment
				(re.compile(r'\#[\w\W]*?(\n|$)'), "preprocessor", 0),		# preprocessor
				(re.compile(r'[_a-zA-Z][_a-zA-Z0-9]*'), None, 0),		# identifier
				(re.compile(r'"(\\.|.)*?("|$|\n)'), "string", 0),		# double-quote string
				(re.compile(r'\'(\\.|.)(\'|$|\n)'), "string", 0),		# char
			]
		]
		
	def getContextualNewline(self, before, after):
		lastLine = before.split("\n")[-1]
		tabDepth = len(lastLine) - len(lastLine.lstrip("\t"))
		beforeText = "\t" * tabDepth
		afterText = ""
		
		notabs = lastLine.lstrip("\t")
		
		if before.endswith("{"):
			afterText = "\n" + beforeText
			beforeText += "\t"
			if not after.startswith("}"):
				afterText += "}"
		elif before.endswith("/**"):
			afterText = "\n" + beforeText
			beforeText += " * "
			afterText += " */"
		elif notabs.startswith(" * "):
			beforeText += " * "

		return (beforeText, afterText)
	
	def isForFile(self, filename, content):
		if filename.endswith(".glsl"):
			return True
	
	def getIconName(self, filename, content):
		return "GLSL"
