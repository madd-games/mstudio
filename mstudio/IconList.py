"""
	Madd Studio

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

import sys, os

MSTUDIO_DATA_BASE = os.path.dirname(__file__) + "/../data"

iconList = {
	"Folder":		MSTUDIO_DATA_BASE + "/images/folder.png",
	"Unknown":		MSTUDIO_DATA_BASE + "/images/unknown.png",
	"Python":		MSTUDIO_DATA_BASE + "/images/python.png",
	"C":			MSTUDIO_DATA_BASE + "/images/c.png",
	"C/C++ Header":		MSTUDIO_DATA_BASE + "/images/h.png",
	"C++":			MSTUDIO_DATA_BASE + "/images/cpp.png",
	"Perun":		MSTUDIO_DATA_BASE + "/images/perun.png",
	"MBS":			MSTUDIO_DATA_BASE + "/images/mbs.png",
	"HTML":			MSTUDIO_DATA_BASE + "/images/html.png",
	"CSS":			MSTUDIO_DATA_BASE + "/images/css.png",
	"JSON":			MSTUDIO_DATA_BASE + "/images/json.png",
	"JavaScript":		MSTUDIO_DATA_BASE + "/images/js.png",
	"Shell":		MSTUDIO_DATA_BASE + "/images/shell.png",
	"Madd Studio Logo":	MSTUDIO_DATA_BASE + "/images/mstudio.png",
	"Project":		MSTUDIO_DATA_BASE + "/images/project.png",
	"SQL":			MSTUDIO_DATA_BASE + "/images/sql.png",
	"GLSL":			MSTUDIO_DATA_BASE + "/images/glsl.png"
}

fileIcons = {
	".py":			"Python",
	".c":			"C",
	".h":			"C/C++ Header",
	".hpp":			"C/C++ Header",
	".cpp":			"C++",
	".per":			"Perun",
	".mbs":			"MBS",
	".html":		"HTML",
	".htm":			"HTML",
	".css":			"CSS",
	".scss":		"CSS",
	".json":		"JSON",
	".js":			"JavaScript",
	".sh":			"Shell",
	".php":			"HTML",
	".sql":			"SQL",
	".glsl":		"GLSL"
}
