"""
	Madd Studio

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

"""Parser for `mbs.meta` files.

To use this, instantiate it, call the `on()` method to set up handlers for metadata commands,
and finally call `feed()` to specify an `mbs.meta` file to parse. Any errors, including whether
the file exists, are silently ignored.
"""
class MBSMetaParser:
	def __init__(self):
		self.handlers = {}
	
	"""Add a handler for a meta command.
	
	The callback is called with `handler(value)` every time the command is encountered.
	"""
	def on(self, cmd, handler):
		self.handlers[cmd] = handler
	
	"""Feed a file into the parser."""
	def feed(self, filename):
		lines = None
		try:
			f = open(filename, "r")
			lines = f.read().splitlines()
			f.close()
		except:
			return
		
		for line in lines:
			try:
				cmd, value = line.split(" ", 1)
				self.handlers[cmd](value)
			except:
				pass