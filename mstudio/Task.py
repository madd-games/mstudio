"""
	Madd Studio

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

from threading import Thread, Lock
from subprocess import Popen, PIPE, STDOUT

"""Represents a task running in the background.

...and allows the GUI to access output.
"""
class Task:
	def __init__(self, cwd, args):
		try:
			args.remove('')
		except:
			pass
		self.proc = Popen(args, cwd=cwd, stdin=PIPE, stdout=PIPE, stderr=STDOUT)
		self.lock = Lock()
		self.data = ""
		self.status = None
		self.thread = Thread(target=self.threadFunc)
		self.thread.start()
	
	def threadFunc(self):
		while True:
			chunk = self.proc.stdout.read(512).decode('utf-8')
			if chunk == "":
				break
			
			self.lock.acquire()
			self.data += chunk
			self.lock.release()
		
		status = self.proc.wait()
		self.lock.acquire()
		self.status = status
		self.lock.release()
		
	def checkStatus(self):
		self.lock.acquire()
		data = self.data
		self.data = ""
		status = self.status
		self.lock.release()
		
		return (data, status)
