"""
	Madd Studio

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

from mstudio.UserConfig import *
from mstudio.EditorMode import *
from mstudio.Tokenizer import *
from mstudio.EditorModeList import editorModeList
from gettext import *
import wx
import wx.stc
import sys, os

"""Code editor widget.

This widget handles all the code editing.
"""
class CodeEditor(wx.stc.StyledTextCtrl):
	def __init__(self, parent, mainFrame):
		wx.stc.StyledTextCtrl.__init__(self, parent)
		self.count = 0
		self.semanticHighlightTimer = None
		self.SetEOLMode(wx.stc.STC_EOL_LF)
		self.lastToker = None
		self.SetUseTabs(True)
		self.SetTabWidth(8)
		self.SetLexer(wx.stc.STC_LEX_CONTAINER)
		self.file = None
		self.reloadConfig()
		self.Bind(wx.stc.EVT_STC_STYLENEEDED, self.onUpdate)
		self.Bind(wx.stc.EVT_STC_CHARADDED, self.onCharAdded)
		self.SetValue("No file was selected yet!\nPlease select one from the Navigator.")
		self.Disable()
		self.mainFrame = mainFrame
		self.SetMarginWidth(1, 50)
		self.SetMarginType(1, wx.stc.STC_MARGIN_NUMBER)
		self.SetSelBackground(True, wx.Colour(*userConfig["CodeEditor.SelectionBackground"]))
		self.SetWrapMode(wx.stc.STC_WRAP_WORD)

	def resetSemanticHighlightTimer(self):
		if self.semanticHighlightTimer is not None:
			self.semanticHighlightTimer.Stop()
			self.semanticHighlightTimer = None
		
		if userConfig["CodeEditor.SemanticHighlighting"]:
			def doHighlight():
				self.mainFrame.onSemanticHighlight(None)
			self.semanticHighlightingTimer = wx.CallLater(1000, doHighlight)

	def onCharAdded(self, e):
		if self.file is None:
			return
		
		pos = self.GetCurrentPos()
		code = self.GetValue()
		
		before = code[:pos]
		after = code[pos:]
		
		beforeText, afterText = self.file.getMode().getContextualAutocomplete(before, after)
		self.InsertText(pos, beforeText)
		pos += len(beforeText)
		self.GotoPos(pos)
		self.InsertText(pos, afterText)

	def processSemanticTokens(self, tokens):
		# TODO: check if still up-to-date
		for token in tokens:
			self.StartStyling(token.startPos, 0x1F);
			self.SetStyling(token.length, self.attrs[token.attrName])
		
	def beginSemanticHighlight(self, buildDir):
		def onTokensReceived(tokens):
			def innerCall():
				self.processSemanticTokens(tokens)
			wx.CallAfter(innerCall)
		self.file.getMode().beginSemanticHighlight(buildDir, os.path.basename(self.file.filename), self.GetValue(), onTokensReceived)
		
	def reloadConfig(self):
		self.SetForegroundColour(wx.Colour(*userConfig["CodeEditor.Foreground"]))
		
		self.attrNull = 0
		self.attrKeyword = 1
		self.attrString = 2
		self.attrNumber = 3
		self.attrUserType = 4
		self.attrComment = 5
		self.attrPreprocessor = 6
		self.attrError = 7
		self.attrLocalVar = 8
		self.attrField = 9
		self.attrMethod = 10
		
		self.StyleSetFont(wx.stc.STC_STYLE_DEFAULT, font=wx.Font(userConfig["CodeEditor.FontSize"], wx.DEFAULT, wx.NORMAL, wx.NORMAL, False, userConfig["CodeEditor.FontFamily"]))
		self.StyleSetBackground(wx.stc.STC_STYLE_DEFAULT, wx.Colour(*userConfig["CodeEditor.Background"]))
		self.StyleSetBackground(self.attrNull, wx.Colour(*userConfig["CodeEditor.Background"]))
		
		self.StyleSetForeground(self.attrNull, wx.Colour(*userConfig["CodeEditor.Foreground"]))
		self.StyleSetForeground(self.attrKeyword, wx.Colour(*userConfig["CodeEditor.Syntax.Keyword"]))
		self.StyleSetForeground(self.attrString, wx.Colour(*userConfig["CodeEditor.Syntax.String"]))
		self.StyleSetForeground(self.attrNumber, wx.Colour(*userConfig["CodeEditor.Syntax.Number"]))
		self.StyleSetForeground(self.attrUserType, wx.Colour(*userConfig["CodeEditor.Syntax.UserType"]))
		self.StyleSetForeground(self.attrComment, wx.Colour(*userConfig["CodeEditor.Syntax.Comment"]))
		self.StyleSetForeground(self.attrPreprocessor, wx.Colour(*userConfig["CodeEditor.Syntax.Preprocessor"]))
		self.StyleSetForeground(self.attrError, wx.Colour(*userConfig["CodeEditor.Syntax.Error"]))
		self.StyleSetForeground(self.attrLocalVar, wx.Colour(*userConfig["CodeEditor.Syntax.LocalVar"]))
		self.StyleSetForeground(self.attrField, wx.Colour(*userConfig["CodeEditor.Syntax.Field"]))
		self.StyleSetForeground(self.attrMethod, wx.Colour(*userConfig["CodeEditor.Syntax.Method"]))
		
		self.attrs = {
			"keyword": self.attrKeyword,
			"string": self.attrString,
			"number": self.attrNumber,
			"userType": self.attrUserType,
			"comment": self.attrComment,
			"preprocessor": self.attrPreprocessor,
			"error": self.attrError,
			"localVar": self.attrLocalVar,
			"field": self.attrField,
			"method": self.attrMethod
		}
		
		for attrID in self.attrs.values():
			self.StyleSetBackground(attrID, wx.Colour(*userConfig["CodeEditor.Background"]))

		if self.file is None:
			self.SetValue(self.GetValue())
		
	def setFile(self, efile):
		content = efile.read()
		if content is not None:
			f = self.file
			self.file = None
			if f is not None:
				f.lastCursorPos = self.GetInsertionPoint()
			self.Enable()
			self.SetValue(content)

			self.GotoPos(efile.lastCursorPos)

			self.file = efile
			self.onUpdate(None)

			bname = os.path.basename(efile.filename)
			self.mainFrame.SetTitle("%s (%s) - Madd Studio" % (bname, efile.filename))
			self.mainFrame.setActiveProject(efile.project)
		
	def onEnter(self, e):
		if self.file is None:
			return
		
		pos = self.GetInsertionPoint()
		code = self.GetValue()
		
		before = code[:pos]
		after = code[pos:]
		
		beforeText, afterText = self.file.getMode().getContextualNewline(before, after)
		self.WriteText(beforeText)
		pos = self.GetInsertionPoint()
		self.WriteText(afterText)
		self.SetInsertionPoint(pos)
	
	def setModeManually(self, modeName):
		self.file.setMode(editorModeList[modeName])
		userConfig["CodeEditor.FileModes"][self.file.filename] = modeName
		SaveWorkspace()
		
		self.lastToker = None
		self.onUpdate(None)
		
	def onUpdate(self, e):
		if self.file is None:
			if e is not None:
				e.Skip()
			return

		code = self.GetValue()
		toker = Tokenizer(code)
		
		wantedStartPos = 0
		if e is not None:
			wantedStartPos = self.GetEndStyled()
		
		if self.lastToker is not None:
			for startPos, endPos, attr, newState in self.lastToker.tokens:
				if endPos >= wantedStartPos:
					break
				else:
					toker.tokens.append((startPos, endPos, attr, newState))
					toker.cursor = endPos

		actualStartPos = toker.cursor
		self.StartStyling(toker.cursor, 0x1F)
		toker.endPos = -1
		if e is not None:
			toker.endPos = e.GetPosition()
			self.SetStyling(toker.endPos-toker.cursor, 0)
		else:
			self.SetStyling(len(code)-toker.cursor, 0)
		
		self.file.getMode().colorize(toker)
		self.file.update(code)
		
		for startPos, endPos, attr, newState in toker.tokens:
			if startPos >= actualStartPos:
				self.StartStyling(startPos, 0x1F)
				self.SetStyling(endPos-startPos, self.attrs[attr])
		
		self.lastToker = toker
		if e is not None:
			e.Skip()

		self.resetSemanticHighlightTimer()