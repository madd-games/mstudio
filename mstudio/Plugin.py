"""
	Madd Studio

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

from zipfile import ZipFile
from mstudio.UserConfig import *
import sys, os
import json
import importlib

"""Represents a plugin.

This class must be subclassed by every plugin.
"""
class Plugin(object):
	def __init__(self):
		pass
	
	"""Called during menu bar creation in the main window."""
	def initMenuBar(self, mainFrame, menuBar):
		pass

loadedPlugins = []

def InitPluginFile(fullpath):
	zip = ZipFile(fullpath)
	f = zip.open("plugin.json")
	pluginData = json.load(f)
	f.close()
	zip.close()
	
	sys.path.append(fullpath)
	
	mod = importlib.import_module(pluginData["Module"])
	cls = getattr(mod, pluginData["Class"])
	plugin = cls()
	plugin.name = pluginData["Name"]
	plugin.path = fullpath
	
	loadedPlugins.append(plugin)

def InitPluginsFrom(dirname):
	fileList = []
	try:
		fileList = os.listdir(dirname)
	except:
		return
	
	for name in fileList:
		if name.endswith(".zip"):
			fullpath = os.path.join(dirname, name)
			try:
				InitPluginFile(fullpath)
			except Exception as e:
				print("Failed to load plugin %s: %s" % (fullpath, str(e)))
			
def InitPlugins():
	InitPluginsFrom("/usr/lib/mstudio/plugins")
	InitPluginsFrom(userPluginsPath)

def PluginInitMenuBar(mainFrame, menuBar):
	for plugin in loadedPlugins:
		plugin.initMenuBar(mainFrame, menuBar)
