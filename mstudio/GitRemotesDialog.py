"""
	Madd Studio

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

import wx

"""Git remotes dialog."""
class GitRemotesDialog(wx.Dialog):
	def __init__(self, mainFrame, remotes):
		wx.Dialog.__init__(self, mainFrame)
		self.SetTitle("Remote repositories")
		self.mainFrame = mainFrame
		
		self.COL_NAME, self.COL_URL = range(2)
		self.ID_ADD = self.NewControlId()
		self.ID_REMOVE = self.NewControlId()
		
		masterSizer = wx.BoxSizer(wx.VERTICAL)
		masterSizer.SetMinSize(wx.Size(600, 300))
		
		self.remotesTable = wx.ListCtrl(self, style=wx.LC_REPORT)
		masterSizer.Add(self.remotesTable, 1, wx.EXPAND | wx.ALL, 10)
		
		self.remotesTable.InsertColumn(self.COL_NAME, "Name")
		self.remotesTable.InsertColumn(self.COL_URL, "URL")

		self.remotesTable.SetColumnWidth(self.COL_NAME, 150)
		self.remotesTable.SetColumnWidth(self.COL_URL, 440)
		
		for key, value in remotes.items():
			self.remotesTable.Append((key, value))
		
		ctrlSizer = wx.BoxSizer(wx.HORIZONTAL)
		masterSizer.Add(ctrlSizer, 0, (wx.ALL ^ wx.TOP) | wx.EXPAND, 10)
		
		self.txtName = wx.TextCtrl(self)
		ctrlSizer.Add(self.txtName, 1, wx.RIGHT, 10)
		self.txtName.SetHint("New remote name...")
		
		self.txtURL = wx.TextCtrl(self)
		ctrlSizer.Add(self.txtURL, 1, wx.RIGHT, 10)
		self.txtURL.SetHint("New remote URL...")
		
		self.btnAdd = wx.Button(self, self.ID_ADD, label="Add")
		ctrlSizer.Add(self.btnAdd, 0, wx.RIGHT, 10)
		self.btnAdd.Disable()
		
		sl = wx.StaticLine(self, style=wx.LI_VERTICAL)
		ctrlSizer.Add(sl, 0, wx.RIGHT, 10)
		
		self.btnRemove = wx.Button(self, self.ID_REMOVE, label="Remove selected")
		ctrlSizer.Add(self.btnRemove, 0, 0, 0)
		self.btnRemove.Disable()
		
		btnSizer = self.CreateSeparatedButtonSizer(wx.CLOSE)
		masterSizer.Add(btnSizer, 0, (wx.ALL ^ wx.TOP) | wx.EXPAND, 10)
		
		masterSizer.SetSizeHints(self)
		self.SetSizer(masterSizer)
		
		self.Bind(wx.EVT_TEXT, self.onTextUpdated)
		self.Bind(wx.EVT_BUTTON, self.onAdd, id = self.ID_ADD)
		self.Bind(wx.EVT_BUTTON, self.onRemove, id = self.ID_REMOVE)
		self.Bind(wx.EVT_LIST_ITEM_SELECTED, self.onItemSelected)
		self.Bind(wx.EVT_LIST_ITEM_DESELECTED, self.onItemDeselected)
	
	def onTextUpdated(self, e):
		name = self.txtName.GetValue()
		url = self.txtURL.GetValue()
		
		if name == "" or " " in name or url == "" or " " in url:
			self.btnAdd.Disable()
		else:
			self.btnAdd.Enable()
	
	def onItemDeselected(self, e):
		self.btnRemove.Disable()
		
	def onItemSelected(self, e):
		self.btnRemove.Enable()
		
	def onAdd(self, e):
		name = self.txtName.GetValue()
		url = self.txtURL.GetValue()
		
		try:
			self.mainFrame.git("remote", "add", name, url)
			self.remotesTable.Append((name, url))
			self.txtName.SetValue("")
			self.txtURL.SetValue("")
			self.btnAdd.Disable()
		except Exception as e:
			wx.MessageBox(str(e), "Madd Studio", wx.ICON_ERROR)
	
	def onRemove(self, e):
		i = self.remotesTable.GetFirstSelected()
		name = self.remotesTable.GetItem(i).GetText()
		
		try:
			self.mainFrame.git("remote", "rm", name)
			self.remotesTable.DeleteItem(i)
			self.btnRemove.Disable()
		except Exception as e:
			wx.MessageBox(str(e), "Madd Studio", wx.ICON_ERROR)