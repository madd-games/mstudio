"""
	Madd Studio

	Copyright (c) 2019, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

from mstudio.UserConfig import *
from threading import Thread
from queue import Queue, Empty
from mstudio.Task import Task
import wx
import subprocess

class BuildPanel(wx.Notebook):
	def __init__(self, parent, mainFrame):
		wx.Notebook.__init__(self, parent)
		self.buildRunning = False
		self.mainFrame = mainFrame
		
		self.COL_SEVERITY, self.COL_LOCATION, self.COL_MESSAGE = range(3)
		self.TAB_CONSOLE, self.TAB_DIAG = range(2)
		
		self.console = wx.TextCtrl(self, style=wx.TE_MULTILINE|wx.TE_READONLY)
		style = wx.TextAttr()
		style.SetFont(wx.Font(userConfig["CodeEditor.FontSize"], wx.DEFAULT, wx.NORMAL, wx.NORMAL, False, userConfig["CodeEditor.FontFamily"]))
		self.console.SetDefaultStyle(style)
		self.AddPage(self.console, "Console")
		
		self.diagTable = wx.ListCtrl(self, style=wx.LC_REPORT)
		self.AddPage(self.diagTable, "Diagnostics")
		
		self.diagTable.InsertColumn(self.COL_SEVERITY, "Severity")
		self.diagTable.InsertColumn(self.COL_LOCATION, "Location")
		self.diagTable.InsertColumn(self.COL_MESSAGE, "Message")
		
		self.diagTable.SetColumnWidth(self.COL_SEVERITY, 80)
		self.diagTable.SetColumnWidth(self.COL_LOCATION, 150)
		self.diagTable.SetColumnWidth(self.COL_MESSAGE, 500)
		
		self.diagData = {}
		self.Bind(wx.EVT_LIST_ITEM_ACTIVATED, self.onItemActivated)

	def onItemActivated(self, e):
		index = e.GetIndex()
		if index in self.diagData:
			fullpath, lineno, offset = self.diagData[index]
			self.mainFrame.navigator.goToLocation(fullpath, lineno, offset)
	
	def pollMake(self):
		data, status = self.task.checkStatus()
		self.console.AppendText(data)
		
		if status is not None:
			self.console.AppendText("\nMake completed with status: %d\n" % status)
			if status != 0:
				wx.MessageBox("Build failed: make exited with status %d!" % status, "Madd Studio", wx.ICON_ERROR)
			else:
				self.console.AppendText("\n=== BUILD SUCCESSFUL ===\n")
			self.end()
		else:
			wx.CallLater(200, self.pollMake)

	def runMake(self):
		self.console.AppendText("\n=== RUNNING MAKE ===\n")
		
		try:
			self.task = Task(self.buildConfig["Dir"], ["make"])
			wx.CallLater(200, self.pollMake)
		except Exception as e:
			self.console.AppendText("Cannot run 'make': %s\n" % str(e))
			wx.MessageBox("Failed to run 'make': %s" % (str(e)))
			self.end()
	
	def pollConfig(self):
		data, status = self.task.checkStatus()
		self.console.AppendText(data)
		
		if status is not None:
			self.console.AppendText("\nConfiguration ended with status: %d\n" % status)
			if status != 0:
				wx.MessageBox("Configuration failed: configure exited with status %d!" % status, "Madd Studio", wx.ICON_ERROR)
				self.end()
			else:
				self.console.AppendText("Running 'make'\n")
				self.runMake()
		else:
			wx.CallLater(200, self.pollConfig)
	
	def end(self):
		lines = self.console.GetValue().splitlines()
		haveDiags = False
		for line in lines:
			pieces = line.split(":", 4)
			if len(pieces) == 5:
				filename, lineno, offset, kind, msg = pieces
				ok = False
				try:
					lineno = int(lineno)
					offset = int(offset)
					ok = True
				except:
					ok = False
				if not ok:
					continue
				fullpath = os.path.abspath(os.path.join(self.buildConfig["Dir"], filename))
				index = self.diagTable.InsertItem(self.diagTable.GetItemCount(), kind.strip().upper())
				self.diagTable.SetItem(index, self.COL_LOCATION, "%s:%d:%d" % (os.path.basename(fullpath), lineno, offset))
				self.diagTable.SetItem(index, self.COL_MESSAGE, msg)
				self.diagData[index] = (fullpath, lineno, offset)
				haveDiags = True
				
		self.buildRunning = False
		self.mainFrame.buildChoice.Enable()
		if haveDiags:
			self.SetSelection(1)
		
	def runBuild(self, project, configName):
		self.buildRunning = True
		self.mainFrame.buildChoice.Disable()
		self.SetSelection(0)
		
		self.console.SetValue("")
		self.diagTable.DeleteAllItems()
		self.diagData = {}
		
		self.SetSelection(self.TAB_CONSOLE)
		
		self.console.AppendText("===== BEGIN BUILD `%s' =====\n" % configName)
		
		configFile = os.path.join(project.info["ProjectDir"], "configure")
		configMTime = 0
		
		try:
			configMTime = os.path.getmtime(configFile)
		except Exception as e:
			wx.MessageBox("ERROR: Cannot get the modification time of configure file `%s': %s\n" % (configFile, str(e)), "Madd Studio",
					wx.ICON_ERROR)
			self.end()
			return
		
		buildConfig = project.info["BuildConfigs"][configName]
		makefile = os.path.join(buildConfig["Dir"], "Makefile")
		self.buildConfig = buildConfig
		
		makefileOutdated = True
		try:
			if os.path.getmtime(makefile) > configMTime:
				makefileOutdated = False
		except:
			makefileOutdated = True
		
		if makefileOutdated:
			self.console.AppendText("Makefile is older than 'configure'; forcing full rebuild\n")
			
			try:
				otherArgs = buildConfig["ConfigArgs"].split(' ')
				while '' in otherArgs:
					otherArgs.remove('')
				self.task = Task(buildConfig["Dir"], ["mbs", configFile, *otherArgs])
				wx.CallLater(200, self.pollConfig)
			except Exception as e:
				self.console.AppendText("Cannot start the 'configure' script: %s\n" % str(e))
				wx.MessageBox("Failed to start %s: %s" % (configFile, str(e)))
				self.end()
		else:
			self.console.AppendText("Makefile is up-to-date; running 'make'\n")
			self.runMake()
